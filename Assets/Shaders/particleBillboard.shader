// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/particleQuad" 
{

 	Properties 
 	{
        _Color ("Main Color", Color) = (0, 1, 1,0.3)
        //_SpeedColor ("Speed Color", Color) = (1, 0, 0, 0.3)
        _SpeedColorOne ("Speed Color Src 1", Color) = (1, 0, 0, 0.3)
        _SpeedColorTwo ("Speed Color Src 2", Color) = (1, 0, 0, 0.3)
        _colorRadiusScale ("Render Color Radius Scale", float) = 60
		//[NoScaleOffset] 
		_MainTex("Texture", 2D) = "white" {}
		_FocusTex("Texture", 2D) = "white" {}

	}

		SubShader
	{
		Pass
		{

			//Blend SrcFactor(* Generated color) + DstFactor(* color already on screen)
			//BlendOp Add
			//Blend SrcColor DstAlpha

			//Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency
			//Blend One OneMinusSrcAlpha // Premultiplied transparency
			//Blend One One // Additive
			//Blend OneMinusDstColor One // Soft Additive
			//Blend DstColor Zero // Multiplicative
			//Blend DstColor SrcColor // 2x Multiplicative

			Blend SrcAlpha one
			//Blend SrcAlpha OneMinusSrcAlpha
			//Blend SrcAlpha OneMinusSrcColor
			//Blend SrcAlpha DstAlpha
			//Blend SrcAlpha Zero
			//Blend SrcAlpha OneMinusDstAlpha
			Cull Back//Off//Front
			ZTest Always//LEqual
			ZWrite on//off  
			
			CGPROGRAM
			#pragma target 5.0
			
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			// The same particle data structure used by both the compute shader and the shader.
			struct Particle
			{
				half3 position;
				half4 velocity;
				half3 physxParams;
			};
			
			struct FragInput
			{
				float4 color : COLOR;
				float4 position : SV_POSITION;
				float4 uv : TEXCOORD0;
				float distLerp : TEXCOORD1;
			};
			
			// The buffer holding the particles shared with the compute shader.
			StructuredBuffer<Particle> particleBuffer;
			// the buffer holding the vertices for the billboard
			StructuredBuffer<float3> quad_verts;
			
			// Variables from the properties.
			float4 _Color;
			float4 _SpeedColorOne;
			float4 _SpeedColorTwo;
			float _colorRadiusScale;
			sampler2D _MainTex;
			float4 _MainTex_ST;	
			sampler2D _FocusTex;
			float4 _FocusTex_ST;
			//float4x4 _TargetRotationMatrix;
			
			//custom DX11 vertex shader. Get params from Graphics.DrawProcedural(MeshTopology.x, y, particleCount);
			//SV_VertexID: "y", the number of vertices to draw per particle, can make a point or a quad or a sphere.
			//SV_InstanceID: "particleCount", number of particles.
			FragInput vert (uint id : SV_VertexID, uint inst : SV_InstanceID)
			{
				FragInput fragInput;

				// color computation
				float speed = particleBuffer[inst].velocity.w;//length(particleBuffer[inst].velocity.xyz);// +particleBuffer[inst].inertia.w*particleBuffer[inst].inertia / 10);
				//speed = lerp(speed, particleBuffer[inst].inertia.xyz*particleBuffer[inst].inertia.w, particleBuffer[inst].inertia.w);
				fragInput.distLerp = clamp(abs(speed / _colorRadiusScale), 0, 1);

				if(particleBuffer[inst].velocity.w > 0)
				{
					fragInput.color = lerp(_Color, _SpeedColorOne, fragInput.distLerp);
				}
				else
				{
					fragInput.color = lerp(_Color, _SpeedColorTwo, fragInput.distLerp);
				}



				float3 v_oPos = particleBuffer[inst].position + mul(unity_WorldToObject, mul(unity_CameraToWorld, quad_verts[id]));
				
				fragInput.uv = float4((quad_verts[id] +0.5) ,1);

				//fragInput.position = mul(UNITY_MATRIX_MVP, mul(_TargetRotationMatrix, float4(v_oPos, 1)));
				fragInput.position = UnityObjectToClipPos(float4(v_oPos,1));
				


				return fragInput;
			}
			

			float4 frag (FragInput fragInput) : COLOR
			{	

				float4 color = fragInput.color * lerp(  tex2D(_MainTex,fragInput.uv * _MainTex_ST.xy + _MainTex_ST.zw), 
														tex2D(_FocusTex,fragInput.uv * _FocusTex_ST.xy + _FocusTex_ST.zw), 
														fragInput.distLerp);


				return color;
			}
			
			ENDCG
		
		}
	}

	Fallback Off
}
