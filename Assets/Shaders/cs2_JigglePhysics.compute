#pragma kernel main

// The same particle data structure used by both the compute shader and the shader.
struct Particle
{
	float3 position;
	float4 velocity;
	float3 physxParams;	
};

// The buffer holding the particles shared with the display shader.
RWStructuredBuffer<Particle> particleBuffer;



int flush;
//float deltaTime;
float drag;
float verletElasticity;


int interactorCount;
/*
NOTE: Can use instead:
void GetDimensions(
  out uint numStructs,
  out uint stride
);
interactorData.GetDimensions();
*/

struct InteractorData
{
	float3 target;
	int isSculpting;
	half targetStrengh;
	half sprayOrbRadius;
	half snoise;
	half enoise;
};
RWStructuredBuffer<InteractorData> interactorData;

[numthreads(32,1,1)] //3d indexed threads, per warp								
//void main (uint3 id : SV_DispatchThreadID, uint3 groupId : SV_GroupID)
void main (uint3 id : SV_DispatchThreadID)
{
	// particleBuffer[id.x] is the particle this thread must Update, according to the thread ID.
	// id.x ---> x is x in (32, 1, 1)
	//http://msdn.microsoft.com/en-us/library/windows/desktop/ff471566(v=vs.85).aspx

	//this for a pass that prepares the particle buffer for switching from compute shader to compute shader. Certain CSs use certain params for different things. This prevents freakout. 
	if(flush == 1)
	{
		particleBuffer[id.x].velocity = float4(0,0,0,0);
		particleBuffer[id.x].physxParams = particleBuffer[id.x].position;
		return;
	}

	float3 newTarget = float3(0,0,0);
	bool wasSculpted = false;
	//particleBuffer[id.x].physxParams.x = 0;
	//particleBuffer[id.x].physxParams.y = 0;
	particleBuffer[id.x].velocity.w = 0;

	for(int itr = 0; itr < interactorCount; itr++)
	{
		if(interactorData[itr].targetStrengh == 0)
		{
			break;
		}

		float3 dir = normalize(interactorData[itr].target - particleBuffer[id.x].position.xyz);
		float dist = abs(distance(interactorData[itr].target, particleBuffer[id.x].position.xyz))*0.75;
		//float facing = dot(dir, target);

		//this keeps the particle scatter in check
		if(dist < interactorData[itr].sprayOrbRadius)
		{
			dist = interactorData[itr].sprayOrbRadius;
		}

		//TODO: maybe check if dist is large enough to not affect this particle and bail early
		//But there's no async magic going on here (yet) so I'm pretty sure the entire thread block has to wait for all threads to finish before continuing to next pass.
		
		int sgn = 1;
		if(interactorData[itr].targetStrengh<0)
			sgn = -1;

		float falloff = (1+dist*dist);
		float powerTemp = interactorData[itr].targetStrengh / falloff;
		float power = powerTemp * (interactorData[itr].snoise + interactorData[itr].enoise);
		float3 tempForce = power * dir;
		
		if(interactorData[itr].isSculpting == 1)
		{
			//particleBuffer[id.x].velocity.w = length(particleBuffer[id.x].velocity.w * particleBuffer[id.x].velocity.xyz + tempVel);
			newTarget += tempForce;
			//particleBuffer[id.x].velocity.xyz = normalize(particleBuffer[id.x].velocity.xyz + dir);
			//particleBuffer[id.x].physxParams.y += falloff;
			wasSculpted = true;
		}

		//this is also sampled by the render shader.
		particleBuffer[id.x].velocity.w += interactorData[itr].targetStrengh * powerTemp * ((interactorData[itr].snoise)*0.05) *sgn;
	}
	/*
	float extra = 1;
	if(wasSculpted)
	{
		extra = 2;
	}
	*/

	particleBuffer[id.x].physxParams.xyz += newTarget;

	// need to make the appearance of sand-ish clumping. 
	//particleBuffer[id.x].velocity.w -= bounceDir*(particleBuffer[id.x].velocity.w  * verletElasticity + 0.01);

	particleBuffer[id.x].velocity.xyz -= (particleBuffer[id.x].position.xyz - particleBuffer[id.x].physxParams.xyz ) * verletElasticity;// * extra;
	
	particleBuffer[id.x].velocity.xyz *= 1 - drag;

	particleBuffer[id.x].position.xyz += particleBuffer[id.x].velocity.xyz+0.1;
}
