// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/DepthClearer"
{
	
    Properties
    {
		 _Color ("Main Color", Color) = (0, 1, 1,0.3)
    }

  
    //CG/HLSL shader used on all other devices with shader support

    SubShader
    {
    	

        Tags {"IgnoreProjector"="True" "RenderType"="Opaque" "Queue" = "Geometry-1"}
		//Tags {"Queue" = "Geometry+500"}
		
		
		
        Pass
        {
			
            //Tags {"LightMode" = "Always" }
			Lighting Off
            //Cull Off
			ZTest Always//LEqual
			ZWrite on
			//Blend SrcAlpha OneMinusSrcAlpha
			//Blend One One

			ColorMask 0

            CGPROGRAM
                #pragma vertex vert

                #pragma fragment frag

                #include "UnityCG.cginc" 

				
                float4 _Color;
				 
                struct appdata_base2
                {
                    float4 vertex : POSITION;

                }; 

                struct v2f
                {
                    float4 pos : POSITION;
                    
                };

 

                v2f vert(appdata_base2 v)
                {
                    v2f o;

                    //o.pos = mul(UNITY_MATRIX_MVP, v.vertex); 
					
                    o.pos = UnityObjectToClipPos(mul(unity_WorldToObject, mul(unity_CameraToWorld, float4(v.vertex.x*10000, v.vertex.y*10000, 1000, 1)))); 


                    return o;
                }



                float4 frag(v2f i) : COLOR 
                { 

                    return _Color;

                }

            ENDCG
        }



    }
}