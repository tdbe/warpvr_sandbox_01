// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/BrushStrength"
{
	
    Properties
    {
		_BgAlpha("Background Alpha", Range(0, 1)) = 0.8
        _CPoint("Contrast Point", Range(-0.5, 1.5)) = 0.11
        _Contrast("Contrast", Range(1, 5)) = 1
        
      	_BrushTex ("Brush Texture (RGB)", 2D) = "white" {}
      	_Mask ("Culling Mask", 2D) = "white" {}
    }

  
    //CG/HLSL shader used on all other devices with shader support

    SubShader
    {
    	

        Tags {"IgnoreProjector"="True" "RenderType"="Opaque" "Queue" = "Transparent"}
		//Tags {"Queue" = "Geometry+500"}
		
		
		
        Pass
        {
			
            //Tags {"LightMode" = "Always" }
			Lighting Off
            //Cull Off
			ZTest Always//LEqual
			ZWrite off
			Blend SrcAlpha OneMinusSrcAlpha
			//Blend One One

            CGPROGRAM
                #pragma vertex vert

                #pragma fragment frag

                #include "UnityCG.cginc" 

				
                uniform float _BgAlpha;
                uniform float _CPoint;
                uniform float _Contrast;
   
                uniform sampler2D _Mask;
                uniform sampler2D _BrushTex_ST; 
				uniform sampler2D _BrushTex;
				 
                struct appdata_base2
                {
                    float4 vertex : POSITION;
                    float4 texcoord : TEXCOORD0;
                    float4 texcoord2 : TEXCOORD1;

                }; 

                struct v2f
                {
                    float4 pos : POSITION;
                    float2 texcoord : TEXCOORD0;
                    
                };

 

                v2f vert(appdata_base2 v)
                {
                    v2f o;

                    o.pos = UnityObjectToClipPos(v.vertex); 

					//o.texcoord.xy = TRANSFORM_TEX(v.texcoord, _BrushTex) ;//http://shattereddeveloper.blogspot.dk/2011/09/handling-texture-tiling-and-offset-in.html
					o.texcoord.xy = v.texcoord.xy;


                    return o;
                }



                float4 frag(v2f i) : COLOR 
                { 

                    half4 color;
                    half4 brushTex = tex2D(_BrushTex, i.texcoord);
                    half4 mask = tex2D(_Mask, i.texcoord);

					mask.rgb = (mask.rgb - _CPoint)*_Contrast + _CPoint; 
 					
					color.rgb = (brushTex.rgb+mask.rgb*_Contrast);//*(brushTex.rgb*_Contrast/2+1);			
					color.a = saturate(mask.a * 30)+(_BgAlpha-1);		
						
					//half bg = (1 - mask.a) * -0.3;

					color.rgb = color.rgb * mask.a;// + half3(bg,bg,bg);

                    return color;

                }

            ENDCG
        }



    }
}