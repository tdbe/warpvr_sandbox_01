// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PointSphere"
{
	
    Properties
    {
		 _Color ("Main Color", Color) = (0, 1, 1,0.3)
    }

  
    //CG/HLSL shader used on all other devices with shader support

    SubShader
    {
    	

        Tags {"IgnoreProjector"="True" "RenderType"="Opaque" "Queue" = "Transparent+10"}
		//Tags {"Queue" = "Geometry+500"}
		
		
		
        Pass
        {
			
            //Tags {"LightMode" = "Always" }
			Lighting Off
            //Cull Off
			ZTest Always//LEqual
			ZWrite on
			Blend SrcAlpha OneMinusSrcAlpha
			//Blend One One

            CGPROGRAM
                #pragma vertex vert

                #pragma fragment frag

                #include "UnityCG.cginc" 

				
                float4 _Color;
				 
                struct appdata_base2
                {
                    float4 vertex : POSITION;
                    float4 texcoord : TEXCOORD0;
                    float4 texcoord2 : TEXCOORD1;

                }; 

                struct v2f
                {
                    float4 pos : POSITION;
                    float2 texcoord : TEXCOORD0;
                    
                };

 

                v2f vert(appdata_base2 v)
                {
                    v2f o;

                    o.pos = UnityObjectToClipPos(v.vertex); 

					//o.texcoord.xy = TRANSFORM_TEX(v.texcoord, _BrushTex) ;//http://shattereddeveloper.blogspot.dk/2011/09/handling-texture-tiling-and-offset-in.html
					o.texcoord.xy = v.texcoord.xy;


                    return o;
                }



                float4 frag(v2f i) : COLOR 
                { 

                    return _Color;

                }

            ENDCG
        }



    }
}