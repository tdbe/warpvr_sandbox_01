﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class AppGUI : MonoBehaviourSingleton<AppGUI>, IReceiveActiveEvent {
    [SerializeField]
    Transform menuPanel;
    [SerializeField]
    Transform brushSettingsTabs;
    [SerializeField]
    Transform brushSettingsTabContent;

    //[SerializeField]
    //Transform brushSettingsTabContentStartPos;
    [SerializeField]
    GameObject brushIconPrefab;
    [SerializeField]
    Transform outerBrushIconParent;
    [SerializeField]
    readonly string innerBrushIconParentTag = "UI_BrushIconContainer";
    [SerializeField]
    readonly string brushSizeSliderTag = "UI_BrushSize_Slider";
    [SerializeField]
    readonly string scatterControlSliderTag = "UI_ScatterControl_Slider";
    [SerializeField]
    readonly string posXSliderTag = "UI_PosX_Slider";
    [SerializeField]
    readonly string posYSliderTag = "UI_PosY_Slider";
    [SerializeField]
    readonly string posZSliderTag = "UI_PosZ_Slider";
    [SerializeField]
    GameObject type_VirtualInputTab;
    [SerializeField]
    GameObject type_ViveInputTab;
    [SerializeField]
    GameObject type_PCInputTab;
    [SerializeField]
    GameObject type_DS4InputTab;
    [SerializeField]
    GameObject type_VirtualInputContent;
    [SerializeField]
    GameObject type_ViveInputContent;
    [SerializeField]
    GameObject type_PCInputContent;
    [SerializeField]
    GameObject type_DS4InputContent;
    [SerializeField]
    ShowPanels showPanels;

    [SerializeField]
    Transform physicsSettingsBtnsParent;
    Dictionary<int, SelectionElement> physicsSettingsBtns = new Dictionary<int, SelectionElement>();

    [SerializeField]
    AnimationCurve brushIconOuterPlacement;

    
    void Awake()
    {
        foreach(Transform btn in physicsSettingsBtnsParent)
        {
            int hc = btn.GetHashCode();
            SelectionElement se = btn.GetComponent<SelectionElement>();
            physicsSettingsBtns.Add(hc,se);
        }
        
    }
    
    // Use this for initialization
    void Start () {

    }



#region warning: heavily prototypish scripting
    public void refreshUIControllerList()
    {
        //find parent for slots
        //delete everything after virtual
        //get prefab for ControlManager.Instance.getControllerFromIndex
        //instantiate that prefab at parent
        //link its sliders with pi
        //createBrushIcon( instance

        int i = 0;
        int start = -1;// brushSettingsTabContentStartPos.GetSiblingIndex();
        foreach (Transform child in brushSettingsTabs)
        {
            if (i > start)
            {
                //removing the listeners is not necessary; unity probably checks if delegate is null. doing it anyway to be sure there's no internal issues.
                Button b = child.gameObject.GetComponent<Button>();
                if (b != null)
                {
                    b.onClick.RemoveAllListeners();
                }
                
                Destroy(child.gameObject);
            }
            i++;
        }
        i = 0;
        foreach (Transform child in brushSettingsTabContent)
        {
            if (i > start)
            {
                //removing the listeners is not necessary; unity probably checks if delegate is null. doing it anyway to be sure there's no internal issues.
                Slider[] sliders = child.GetComponentsInChildren<Slider>();
                for (i = 0; i < sliders.Length; i++)
                {
                    sliders[i].onValueChanged.RemoveAllListeners();
                }
                Button[] buttons = child.GetComponentsInChildren<Button>();
                for (i = 0; i < buttons.Length; i++)
                {
                    buttons[i].onClick.RemoveAllListeners();
                }

                Destroy(child.gameObject);
            }
            i++;
        }
        i = 0;
        foreach (Transform child in outerBrushIconParent)
        {
            if (i > start)
            {
                Destroy(child.gameObject);
            }
            i++;
        }

        int pic = MainGame.Instance.particleInteractorCount;
        int vpic = MainGame.Instance.virtualParticleInteractorCount;
        int total = pic + vpic;
        for (int p = 0; p < total; p++)
        {
            ParticleInteractor particleInteractor;
            if(p< pic)
            {
                particleInteractor = MainGame.Instance.particleInteractors[p];
            }
            else
            {
                particleInteractor = MainGame.Instance.particleInteractorsVirtual[p - pic];
            }
            
            byte id = (byte)Mathf.Clamp(p, 0, 255);
            IStandardizedControl isc = ControlManager.Instance.getControllerFromIndex(ref id);

            GameObject vit = null;
            GameObject vic = null;
            string inputName = " ";
            if (isc == null)//it is a virtual controller with no dedicated input controller; uses interface sliders or a hotkey.
            {
                vit = GameObject.Instantiate(type_VirtualInputTab);
                vit.transform.SetParent(brushSettingsTabs, false);
                vic = GameObject.Instantiate(type_VirtualInputContent);
                vic.transform.SetParent(brushSettingsTabContent, false);
                inputName = "Virtual #" + (id - pic + 1);
                vit.transform.GetChild(0).GetComponent<Text>().text = inputName;
            }
            else if (isc.GetType() == typeof(ViveControlManager))
            {                           
                vit = GameObject.Instantiate(type_ViveInputTab);
                vit.transform.SetParent(brushSettingsTabs, false);
                vic = GameObject.Instantiate(type_ViveInputContent);
                vic.transform.SetParent(brushSettingsTabContent, false);
                inputName = "Vive #" + (id + 1);
                vit.transform.GetChild(0).GetComponent<Text>().text = inputName;
            }
            else if (isc.GetType() == typeof(PCControlManager))
            {
                vit = GameObject.Instantiate(type_PCInputTab);
                vit.transform.SetParent(brushSettingsTabs, false);
                vic = GameObject.Instantiate(type_PCInputContent);
                vic.transform.SetParent(brushSettingsTabContent, false);
                inputName = "PC Input";// + (id+1);
                vit.transform.GetChild(0).GetComponent<Text>().text = inputName;
            }
            else if (isc.GetType() == typeof(DS4ControlManager))
            {
                vit = GameObject.Instantiate(type_DS4InputTab);
                vit.transform.SetParent(brushSettingsTabs, false);
                vic = GameObject.Instantiate(type_DS4InputContent);
                vic.transform.SetParent(brushSettingsTabContent, false);
                inputName = "DS4 #" + (id + 1);
                vit.transform.GetChild(0).GetComponent<Text>().text = inputName;
            }

            //add event linking tab to content
            vit.GetComponent<Button>().onClick.AddListener(
                () => { showPanels.ShowInputTab(vit.transform.GetSiblingIndex()); });// <-- eat clusures, you parameterless UnityEngine.Events! 
            vit.GetComponent<SelectionElement>().parent = menuPanel.gameObject.GetComponent<SelectionTracker>();

            //link sliders with particle interactor
            Slider[] sliders = vic.GetComponentsInChildren<Slider>();
            for(i= 0; i < sliders.Length; i++)
            {
                if (sliders[i].CompareTag(brushSizeSliderTag))
                {
                    particleInteractor.initBrushSizeSliderValue(ref sliders[i]);
                    sliders[i].onValueChanged.AddListener(
                            (float val) => { particleInteractor.piGUI.OnBrushSizeSliderChanged(val); }
                        );
                    //slider.GetComponent<BroadcastActiveEvent>().targets = new GameObject[] { this.gameObject };
                }
                else if (sliders[i].CompareTag(scatterControlSliderTag))
                {
                    particleInteractor.initScatterControlSliderValue(ref sliders[i]);
                    sliders[i].onValueChanged.AddListener(
                            (float val) => { particleInteractor.piGUI.OnScatterControlSliderChanged(val); }
                        );
                }
                else if (sliders[i].CompareTag(posXSliderTag))
                {
                    particleInteractor.initPositionSliderValue(ref sliders[i]);
                    sliders[i].onValueChanged.AddListener(
                            (float val) => { particleInteractor.piGUI.OnMoveXAxisSliderChanged(val); }
                        );
                }
                else if (sliders[i].CompareTag(posYSliderTag))
                {
                    particleInteractor.initPositionSliderValue(ref sliders[i]);
                    sliders[i].onValueChanged.AddListener(
                            (float val) => { particleInteractor.piGUI.OnMoveYAxisSliderChanged(val); }
                        );
                }
                else if (sliders[i].CompareTag(posZSliderTag))
                {
                    particleInteractor.initPositionSliderValue(ref sliders[i]);
                    sliders[i].onValueChanged.AddListener(
                            (float val) => { particleInteractor.piGUI.OnMoveZAxisSliderChanged(val); }
                        );
                }
            }

            
            //Instantiate a brushIconPrefab at location, and make its brushIconMaterial an Instance.
            //the Instance is shared between the one in the menu and the one on screen.
            Transform brushIconContainer = //this is a hack. I know where the GO is in the hierarchy, and I don't want to search for it
            vic.transform.GetChild(1).GetChild(0);
            GameObject brushIcon = Instantiate(brushIconPrefab);
            brushIcon.transform.parent = brushIconContainer;
            brushIcon.transform.position = brushIconContainer.position;
            brushIcon.transform.localScale = Vector3.one;
            brushIcon.transform.rotation = brushIconContainer.rotation;
            brushIcon.transform.GetChild(0).GetComponent<Text>().text = inputName;
            MeshRenderer mr = brushIcon.GetComponent<MeshRenderer>();
            Material bim = mr.material;
            particleInteractor.piGUI.linkBrushIconMaterial(bim);

            GameObject brushIcon2 = Instantiate(brushIcon);
            brushIcon2.transform.parent = outerBrushIconParent;
            float tot = total;
            if (tot == 1)
            {
                tot = 0;
            }
            else if (tot == 2)
            {
                tot = 2;
            }
            else if(tot == 3)
            {
                tot = 2f;
            }
            else if (tot%2==0)
            {
                tot -= 1;
            }

            float half = tot / 2;
            float x = 0 + p - half;
            float y = brushIconOuterPlacement.Evaluate(x);
            float limit = 4.5f;
            x = Mathf.Clamp(x, -limit, limit);
            brushIcon2.transform.localPosition = new Vector3(x, y , 0);
            brushIcon2.transform.localScale = Vector3.one;
            brushIcon2.transform.rotation = outerBrushIconParent.rotation;
            //brushIcon.transform.position = RenderManager.Instance.mainCam.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Screen.width * 0.1f, Screen.height * 0.12f, 4));

        }

        //Refresh unity interface after changes
        //Canvas.ForceUpdateCanvases();
        //brushSettingsTabs.gameObject.SetActive(false);
        //StartCoroutine(doAfterTime(() => { brushSettingsTabs.gameObject.SetActive(true); }, 0.05f));
    }
#endregion

    IEnumerator doActionNextFrame(System.Action action)
    {
        yield return new WaitForEndOfFrame();
        action();
    }

    IEnumerator doAfterTime(System.Action action, float duration)
    {
        yield return new WaitForSeconds(duration);
        action();
    }

    public void ResetAppEvent()
    {
        MainGame.Instance.OnApplicationResetRequest();
    }

    public void ReceiveAwakeEvent(GameObject from, bool active)
    {
    }

    public void ReceiveStartEvent(GameObject from, bool active)
    {
        //If "From" is a SelectionElement, and is a physicsSettingsBtn, highlight it (or not) based on what the current MainGame physicsSettings are.
        SelectionElement btnSelEl;
        if (physicsSettingsBtns.TryGetValue(from.transform.GetHashCode(), out btnSelEl))
        {
            int gpi = (int)MainGame.Instance.physicsSettings.current.name;
            if (gpi == from.transform.GetSiblingIndex())
            {
                btnSelEl.OnSelected();
            }
        }
        else if (from.name.Equals("Gamma"))//didn't have time to make a better system
        {
            //This initializes the slider when the slider becomes active
            //The programatically created sliders (for the brush interactors) have this covered at spawn time.
            from.GetComponent<Slider>().value = RenderManager.Instance.getCamBgDefaultBrightness();
        }
    }

    public void OnGammaValueChanged(float val)
    {
        MainGame.Instance.OnGammaChanged(val);
    }

    public void OpenURL(string url)
    {
        MainGame.Instance.openURL(url);
    }

    public void Quit()
    {
        MainGame.Instance.quit();
    }


    public void SetPhysicsPreset(int index)
    {

        MainGame.Instance.OnPhysicsSettingsChanged((byte)index);
    }


    public void toggleUIElement(GameObject uiElement)
    {
        uiElement.SetActive(!uiElement.activeInHierarchy);
    }

    public void AddVirtualInteractor()
    {
        MainGame.Instance.appendVirtualInteractor();
    }

    public void RemoveVirtualInteractor()
    {
        MainGame.Instance.deleteLastVirtualInteractor();
    }
}
