﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Runtime.InteropServices;

namespace DS4Api
{

    public class DS4Manager
    {
        public const ushort pid = 0x05C4;
        public const ushort vid = 0x054C;

        /// A list of all currently connected Wii Remotes.
        public static List<DS4> Controllers { get { return _Controllers; } }
        private static List<DS4> _Controllers = new List<DS4>();

        public static int FindWiimotes()
        {
            IntPtr ptr = HIDapi.hid_enumerate(vid, pid);
            IntPtr cur_ptr = ptr;

            if (ptr == IntPtr.Zero)
                return 0;

            hid_device_info enumerate = (hid_device_info)Marshal.PtrToStructure(ptr, typeof(hid_device_info));

            //bool found = false;
            int found = 0;

            while (cur_ptr != IntPtr.Zero)
            {
                DS4 remote = null;
                bool fin = false;
                foreach (DS4 r in Controllers)
                {
                    if (fin)
                    {
                        found++;
                        continue;
                    }


                    if (r.hidapi_path.Equals(enumerate.path))
                    {
                        remote = r;
                        fin = true;
                        found++;
                    }
                }

                //if (fin)
                //    found = true;

                if (remote == null)
                {
                    IntPtr handle = HIDapi.hid_open_path(enumerate.path);

                    remote = new DS4(handle, enumerate.path);

                    Debug.Log("Found New DS4 Remote: " + remote.hidapi_path);

                    Controllers.Add(remote);

                    //found = true;

                    // TODO: Initialization (?)
                }

                cur_ptr = enumerate.next;
                if (cur_ptr != IntPtr.Zero)
                {
                    enumerate = (hid_device_info)Marshal.PtrToStructure(cur_ptr, typeof(hid_device_info));
                }
            }

            HIDapi.hid_free_enumeration(ptr);

            return found;
        }

        public static void Cleanup(DS4 remote)
        {
            if (remote != null)
            {
                if (remote.hidapi_handle != IntPtr.Zero)
                    HIDapi.hid_close(remote.hidapi_handle);

                Controllers.Remove(remote);
            }
        }

        public static bool HasWiimote()
        {
            return !(Controllers.Count <= 0 || Controllers[0] == null || Controllers[0].hidapi_handle == IntPtr.Zero);
        }
    }
}