using UnityEngine;
using System.Collections;

/// <summary>
/// TO IMPLEMENT
/// </summary>
public class ViveControlManager : IStandardizedControl
{

    public byte numControllers
    {
        get
        {   
            byte num = 0;
            for(int i = 0; i < m_steamVRControllerReferences.Length; i++)
            {
                if (m_steamVRControllerReferences[i].enabled)
                {
                    num++;
                }
            }
            
            return num;
        }
    }
    SteamVR_TrackedController[] m_steamVRControllerReferences;
    SteamControllerWrapper[] m_steamControllersWrapped;



    public ViveControlManager(SteamVR_TrackedController[] steamVRControllerReferences)
    {
        m_steamVRControllerReferences = steamVRControllerReferences;

        Debug.Log("Num steam controllers: " + m_steamVRControllerReferences.Length);
        for (int i = 0; i < m_steamVRControllerReferences.Length; i++)
        {
            m_steamControllersWrapped[i] = new SteamControllerWrapper();
            m_steamVRControllerReferences[i].TriggerClicked += m_steamControllersWrapped[i].OnTriggerClicked;
            m_steamVRControllerReferences[i].TriggerUnclicked += m_steamControllersWrapped[i].OnTriggerUnclicked;

            m_steamVRControllerReferences[i].MenuButtonClicked += m_steamControllersWrapped[i].OnMenuPressed;
            m_steamVRControllerReferences[i].MenuButtonUnclicked += m_steamControllersWrapped[i].OnMenuUnpressed;


            //steamVRControllerReferences[i].OnGripped += m_steamControllers[i].OnGripPressed;
            //steamVRControllerReferences[i].gri += m_steamControllers[i].OnGripPressed;

            //m_steamVRControllerReferences[i].PadClicked += m_steamControllersWrapped[i].OnPadPressed;
            //m_steamVRControllerReferences[i].PadUnclicked += m_steamControllersWrapped[i].OnPadUnpressed;

        }
    }

    private class SteamControllerWrapper
    {
        public class SCData
        {
            public bool triggerPressedDown;
            public bool gripPressedDown;
            public bool menuPressedDown;
            public bool sizePlusDown;
            public bool sizeMinusDown;
            public bool roughnessPlusDown;
            public bool roughnessMinusDown;
        }
        public SCData data = new SCData();
        public SCData dataLF = new SCData();
        SteamVR_TrackedController svrtc;

        public void OnTriggerClicked(object sender, ClickedEventArgs e)
        {
            //Debug.Log("OnTriggerClicked");
            dataLF.triggerPressedDown = data.triggerPressedDown;
            data.triggerPressedDown = true;
        }

        public void OnTriggerUnclicked(object sender, ClickedEventArgs e)
        {
            //Debug.Log("OnTriggerUnClicked");
            dataLF.triggerPressedDown = data.triggerPressedDown;
            data.triggerPressedDown = false;
        }

        public void OnMenuPressed(object sender, ClickedEventArgs e)
        {
            dataLF.menuPressedDown = data.menuPressedDown;
            data.menuPressedDown = true;
        }

        public void OnMenuUnpressed(object sender, ClickedEventArgs e)
        {
            dataLF.menuPressedDown = data.menuPressedDown;
            data.menuPressedDown = false;
        }

        public void OnGripPressed()
        {
            dataLF.gripPressedDown = data.gripPressedDown;
            data.gripPressedDown = true;
        }

        public void OnGripUnpressed()
        {
            dataLF.gripPressedDown = data.gripPressedDown;
            data.gripPressedDown = false;
        }

        void sizeAndRoughnessPlusMinus()
        {
            SteamVR_Controller.Device device = SteamVR_Controller.Input((int)svrtc.controllerIndex);

            if (device.GetPress(SteamVR_Controller.ButtonMask.Touchpad) &&
                device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad)
            )
            {
                Vector2 touchPos = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
                //device.TriggerHapticPulse((ushort)(132 + rumblePulseDuration * Vector2.Distance(prevTouchpadPos, touchpad)));
                ushort hapticPulseSize = 200;
                ushort hapticPulseRoughness = 300;
                device.TriggerHapticPulse(hapticPulseSize);

                float distThresh = 0.35f;
                float distToUp = Vector2.Distance(Vector2.up, touchPos);
                float distToDown = Vector2.Distance(Vector2.down, touchPos);
                float distToLeft = Vector2.Distance(Vector2.left, touchPos);
                float distToRight = Vector2.Distance(Vector2.right, touchPos);
   
                if (distToUp < distThresh && distToUp < distToDown)
                {
                    dataLF.roughnessPlusDown = data.roughnessPlusDown;
                    dataLF.roughnessMinusDown = data.roughnessMinusDown;
                    data.roughnessPlusDown = true;
                    data.roughnessMinusDown = false;
                    device.TriggerHapticPulse(hapticPulseRoughness);
                }
                else if (distToDown < distThresh && distToUp > distToDown)
                {
                    dataLF.roughnessPlusDown = data.roughnessPlusDown;
                    dataLF.roughnessMinusDown = data.roughnessMinusDown;
                    data.roughnessPlusDown = false;
                    data.roughnessMinusDown = true;
                    device.TriggerHapticPulse(hapticPulseRoughness);
                }
                else
                {
                    dataLF.roughnessPlusDown = data.roughnessPlusDown;
                    dataLF.roughnessMinusDown = data.roughnessMinusDown;
                    data.roughnessPlusDown = false;
                    data.roughnessMinusDown = false;
                }

                if (distToRight < distThresh && distToRight < distToLeft)
                {
                    dataLF.sizePlusDown = data.sizePlusDown;
                    dataLF.sizeMinusDown = data.sizeMinusDown;
                    data.sizePlusDown = true;
                    data.sizeMinusDown = false;
                    device.TriggerHapticPulse(hapticPulseSize);
                }
                else if (distToLeft < distThresh && distToRight > distToLeft)
                {
                    dataLF.sizePlusDown = data.sizePlusDown;
                    dataLF.sizeMinusDown = data.sizeMinusDown;
                    data.sizePlusDown = false;
                    data.sizeMinusDown = true;
                    device.TriggerHapticPulse(hapticPulseSize);
                }
                else
                {
                    dataLF.sizePlusDown = data.sizePlusDown;
                    dataLF.sizeMinusDown = data.sizeMinusDown;
                    data.sizePlusDown = false;
                    data.sizeMinusDown = false;
                }

            }
            else
            {
                dataLF.roughnessPlusDown = data.roughnessPlusDown;
                dataLF.roughnessMinusDown = data.roughnessMinusDown;
                dataLF.sizePlusDown = data.sizePlusDown;
                dataLF.sizeMinusDown = data.sizeMinusDown;

                data.roughnessPlusDown = false;
                data.roughnessMinusDown = false;
                data.sizePlusDown = false;
                data.sizeMinusDown = false;
            }
        }

        public void UpdateCall(SteamVR_TrackedController svrtc)
        {
            this.svrtc = svrtc;

            if (svrtc.gripped)
                OnGripPressed();
            else
                OnGripUnpressed();

            sizeAndRoughnessPlusMinus();
        }
    }

    public void MUpdate()
    {
        for (int i = 0; i < m_steamVRControllerReferences.Length; i++)
        {
            m_steamControllersWrapped[i].UpdateCall(m_steamVRControllerReferences[i]);
        }
    }

    private bool convertButton(ControlManager.InputCode buttonName, byte controllerID, bool useLastFrame)
    {
        SteamControllerWrapper.SCData data;
        if(useLastFrame)
            data = m_steamControllersWrapped[controllerID].dataLF;
        else
            data = m_steamControllersWrapped[controllerID].data;

        switch (buttonName)
        {
            case ControlManager.InputCode.RoughnessButtonPlus:
                return data.roughnessPlusDown;
            case ControlManager.InputCode.RoughnessButtonMinus:
                return data.roughnessMinusDown;
            case ControlManager.InputCode.SizeButtonPlus:
                return data.sizePlusDown;
            case ControlManager.InputCode.SizeButtonMinus:
                return data.sizeMinusDown;
            case ControlManager.InputCode.Grip:
                return data.gripPressedDown;
            case ControlManager.InputCode.Fire:
                return data.triggerPressedDown;
            case ControlManager.InputCode.Invert:
                return data.menuPressedDown;
            default:
                return false;// buttonName.ToString();
        }
    }

    public void GrabParticleVolume(ref ParticleInteractor.SceneRotationSpeed rotationSpeed, byte controllerID, Transform thisInteractor)
    {

        if (GetButtonDown(ControlManager.InputCode.Grip, controllerID))
        {
            MainGame.Instance.particleRootPos.parent = thisInteractor;
        }
        else if (GetButtonUp(ControlManager.InputCode.Grip, controllerID))
        {
            MainGame.Instance.particleRootPos.parent = MainGame.Instance.particleRootPosParent;
        }
    }

    public void Get3DPos(ref bool onGUI, ref Vector3 pos, ref Quaternion rot, ref Rigidbody rigidb, float moveSensitivity, byte controllerID)
    {
        Transform tr = m_steamVRControllerReferences[controllerID].transform;
        pos = tr.position;
        rot = tr.rotation;
        rigidb = null;
    }

    public float GetAxis(ControlManager.InputCode axisName, byte controllerID)
    {
        Debug.LogError("TODO: To IMplement Vive Input Axis Mapping");
        return 0;// convertAxis(axisName, controllerID, data);
    }

    public bool GetButton(ControlManager.InputCode buttonName, byte controllerID)
    {
        
        //Debug.LogError("TODO: To IMplement Vive Input Mapping");
        return convertButton(buttonName, controllerID, false);
    }

    public bool GetButtonUp(ControlManager.InputCode buttonName, byte controllerID)
    {

        bool now = convertButton(buttonName, controllerID, false);
        bool then = convertButton(buttonName, controllerID, true);

        if (then == true && now == false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
        

    public bool GetButtonDown(ControlManager.InputCode buttonName, byte controllerID)
    {
        //Debug.LogError("TODO: To IMplement Vive Input Mapping");

        bool now = convertButton(buttonName, controllerID, false);
        bool then = convertButton(buttonName, controllerID, true);

        if (then == false && now == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
