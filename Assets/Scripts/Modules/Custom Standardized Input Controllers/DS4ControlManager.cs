using UnityEngine;
using System.Collections.Generic;
using DS4Api;

public class DS4ControlManager : IStandardizedControl {

    private float analogDeadzone = 0.25f;
    
    public byte numControllers
    {
        get
        {
            if (DS4Manager.Controllers != null)
                return (byte)DS4Manager.Controllers.Count;
            else return 0;
        }
    }
    private List<DS4Data> data = new List<DS4Data>();
    private List<DS4Data> data_lastFrame = new List<DS4Data>();

    bool statMsgOnce = true;


    public DS4ControlManager()
    {
        Debug.Log("Trying to find DS4 controllers. (if any)");
        DS4Manager.FindWiimotes();

        
        initData();
    }

    void initData()
    {
        getDS4Data(ref data);

        data_lastFrame = new List<DS4Data>();
        for (int i = 0; i < data.Count; i++)
        {
            if (i >= data_lastFrame.Count)
            {
                data_lastFrame.Add(data[i]);
            }
            else
            {
                data_lastFrame[i] = data[i];
            }
        }
    }

    //TODO: Figure out if DS4Manager constantly checks if a controller got removed. And Update data[] accordingly.

    public void GrabParticleVolume(ref ParticleInteractor.SceneRotationSpeed rotationSpeed, byte controllerID, Transform thisInteractor)
    {
        
        if(GetButtonDown(ControlManager.InputCode.Grip, controllerID))
        {
            MainGame.Instance.particleRootPos.parent = thisInteractor;
        }
        else if(GetButtonUp(ControlManager.InputCode.Grip, controllerID))
        {
            MainGame.Instance.particleRootPos.parent = MainGame.Instance.particleRootPosParent;
        }
        
    }

    public void Get3DPos(ref bool onGUI, ref Vector3 pos, ref Quaternion rot, ref Rigidbody rigidb, float moveSensitivity, byte localControllerID)
    {
        handleMovement(ref rigidb, moveSensitivity, localControllerID);
        handleRotation(ref rot, localControllerID);
    }

    private void handleMovement(ref Rigidbody rigidb, float moveSensitivity, byte localControllerID)
    {
        /*
        Vector3 controllerAcc = new Vector3(
            InputManager.Instance.GetAxis(InputManager.InputCode.XAxisAcc, globalControllerIndex),
            InputManager.Instance.GetAxis(InputManager.InputCode.YAxisAcc, globalControllerIndex),
            InputManager.Instance.GetAxis(InputManager.InputCode.ZAxisAcc, globalControllerIndex)
        );

        Vector3 force = Vector3.zero;

        Vector3 deltaAcc = controllerAcc - prevAcc;
        if (Mathf.Abs(deltaAcc.x) > Mathf.Abs(accIgnoreDelta.x))
        {
            force.x = Mathf.Clamp(controllerAcc.x*Time.deltaTime* moveSensitivity,-accClamp.x, accClamp.x);

        }
        if (Mathf.Abs(deltaAcc.y) > Mathf.Abs(accIgnoreDelta.y))
        {
            force.y = Mathf.Clamp(controllerAcc.y * Time.deltaTime * moveSensitivity, -accClamp.y, accClamp.y);
        }
        if (Mathf.Abs(deltaAcc.z) > Mathf.Abs(accIgnoreDelta.z))
        {
            force.z = Mathf.Clamp(controllerAcc.z * Time.deltaTime * moveSensitivity,-accClamp.z, accClamp.z);
        }
        if (!Vector3.Equals(force, Vector3.zero))
        {
            //Debug.Log("force: " + force);
            rigidbody.velocity.Set(0, 0, 0);
            rigidbody.AddForce(force);
        }
        prevAcc = controllerAcc;
        //Can't push ball around reliably at all using the accelerometer.
        */

        Vector3 force = Vector3.zero;
        force.x = GetAxis(ControlManager.InputCode.XAxisMove, localControllerID);
        force.y = -GetAxis(ControlManager.InputCode.YAxisMove, localControllerID);
        force.z = -GetAxis(ControlManager.InputCode.ZAxisMove, localControllerID);

        force = rigidb.transform.right * force.x + rigidb.transform.up * force.y + rigidb.transform.forward * force.z;
        //Debug.Log(force.x * Time.deltaTime * moveSensitivity);

        rigidb.AddForce(force * Time.deltaTime * moveSensitivity);
    }

    private void handleRotation(ref Quaternion rot, byte localControllerID)
    {
        Vector3 euler = new Vector3( 
            GetAxis(ControlManager.InputCode.XAxisOrientation, localControllerID),
            GetAxis(ControlManager.InputCode.ZAxisOrientation, localControllerID),
            GetAxis(ControlManager.InputCode.YAxisOrientation, localControllerID));

        
        rot = Quaternion.Euler(RenderManager.Instance.mainCam.transform.rotation.eulerAngles + euler);
        
        



    }


    public void MUpdate()
    {
        int numcontrollers = DS4Manager.FindWiimotes();
        int currControllers = checkConnectedControllers();

        if (numcontrollers != currControllers)
        {
            for (int i = 0; i < DS4Manager.Controllers.Count; i++)
            {
                DS4Manager.Cleanup(DS4Manager.Controllers[i]);
            }
            DS4Manager.FindWiimotes();
            initData();
        }


        getDS4Data(ref data);

        //checkZeroControllers();
        //test if a controller got removed
        //checkControllerChanged();
        
    }

    int checkConnectedControllers()
    {
        for(int i=0; i< DS4Manager.Controllers.Count; i++)
        {
            if(DS4Manager.Controllers[i].hidapi_handle != System.IntPtr.Zero)//this counts up until the *first* controller that was disconnected
            {
                return i + 1;
            }
        }
        return 0;
    }

    void checkControllerChanged()
    {
        bool anyControllerRemoved = false;

        for(int i=0; i< data.Count; i++)
        {
            Vector2 lstick;
            lstick.x = data[i].lstick[0];
            lstick.y = data[i].lstick[1];
            Vector2 rstick;
            rstick.x = data[i].rstick[0];
            rstick.y = data[i].rstick[1];
            float val = data[i].lstick[0] + data[i].lstick[1] + data[i].rstick[0] + data[i].rstick[1];// + data[i].Orientation.Gyro_Raw.x + data[i].Orientation.Gyro_Raw.y + data[i].Orientation.Gyro_Raw.z;
            //TODO: Figure out how to properly detect if a controller got unplugged. Can't use gyro/acceleration xyz check if 0, because those are 0 if controller is in sleep mode
            //if(Input.GetJoystickNames().Length != ControlManager.Instance.totalNumberOfJoysticks)// <- don't work unless it's xbox
            if (val == 0.0f)
            {
                anyControllerRemoved = true;
                break;
            }
        }

        if (anyControllerRemoved)
        {
            DS4Manager.FindWiimotes();
            initData();
        }
            
    }

    void checkZeroControllers()
    {
        if (!DS4Manager.HasWiimote())
        {
            if (statMsgOnce)
            {
                Debug.LogWarning("No DS4 controllers detected. If you plugged one in, either restart the application, or press Menu button to try to detect again.");
                statMsgOnce = false;
            }


            if (Input.GetButtonDown(ControlManager.InputCode.Menu.ToString()))
            {
                Debug.Log("Trying to find DS4 controllers.");
                int found = DS4Manager.FindWiimotes();
                statMsgOnce = true;
                if (found>0)
                {
                    initData();
                }
            }
            else
                return;
        }
    }

    void getDS4Data(ref List<DS4Data> data)
    {
        

        for (int i = 0; i < DS4Manager.Controllers.Count; i++)
        {
            DS4Data tentative = i >= data.Count ? null : data[i];
            do
            {
                if (i >= data.Count)
                {
                    data.Add(tentative);
                }
                else
                {
                    data[i] = tentative;
                }
                tentative = DS4Manager.Controllers[i].ReadDS4Data();
            }
            while (tentative != null);
        }
    }

    public void m_LateUpdate()
    { 
        if(data.Count != data_lastFrame.Count)
        {
            return;
        }

        for (int i = 0; i < data.Count; i++)
        {
            data_lastFrame[i] = data[i];
        }
    }

    private float remapAnalogValue(float analog, bool positiveOnly, float deadzone)
    {
        float val = (analog / 255) ;
        if (!positiveOnly)
        {
            val = val * 2 - 1;
        }

        if (Mathf.Abs(val) < deadzone)
        {
            val = 0;
        }
        return val;
    }



    private float convertAxis(ControlManager.InputCode axisName, byte controllerID, List<DS4Data> source)
    {
        switch (axisName)
        {
            /*
            case ControlManager.InputCode.FireAxis:
                float val = remapAnalogValue((source[controllerID].rstick[0]), false, 0.5f) + remapAnalogValue((source[controllerID].rstick[1]), false, 0.5f);
                //val += remapAnalogValue(source[controllerID].R2_analog, true, 0.1f);
                return Mathf.Clamp01(val);*/
            case ControlManager.InputCode.XAxisMove:
                return remapAnalogValue(source[controllerID].lstick[0],false, analogDeadzone) + remapAnalogValue(source[controllerID].rstick[0], false, analogDeadzone);
            case ControlManager.InputCode.YAxisMove:
                return remapAnalogValue(source[controllerID].lstick[1], false, analogDeadzone);
            case ControlManager.InputCode.ZAxisMove:
                //return remapAnalogValue(source[controllerID].L2_analog, true, 0.1f) - remapAnalogValue(source[controllerID].R2_analog, true, 0.1f);
                return remapAnalogValue(source[controllerID].rstick[1], false, analogDeadzone);
            case ControlManager.InputCode.XAxisAcc:
                return source[controllerID].Orientation.Accel_Raw.x;
            case ControlManager.InputCode.YAxisAcc:
                return source[controllerID].Orientation.Accel_Raw.z;
            case ControlManager.InputCode.ZAxisAcc:
                return source[controllerID].Orientation.Accel_Raw.y;
            case ControlManager.InputCode.XAxisOrientation:
                return source[controllerID].Orientation.Orientation.eulerAngles.x;
            case ControlManager.InputCode.YAxisOrientation:
                return source[controllerID].Orientation.Orientation.eulerAngles.z;
            case ControlManager.InputCode.ZAxisOrientation:
                return source[controllerID].Orientation.Orientation.eulerAngles.y;
            case ControlManager.InputCode.RoughnessAxis:
                if (source[controllerID].DpadLeft)
                {
                    return -1;
                }
                else if (source[controllerID].DpadRight)
                {
                    return 1;
                }
                else return 0;
            case ControlManager.InputCode.SizeAxis:
                if (source[controllerID].DpadDown)
                {
                    return -1;
                }
                else if (source[controllerID].DpadUp)
                {
                    return 1;
                }
                else return 0;
            default:
                return 0.0f;
        }
    }

    private bool convertButton(ControlManager.InputCode buttonName, byte controllerID, List<DS4Data> source)
    {
        switch (buttonName)
        {
            case ControlManager.InputCode.Fire:
                return source[controllerID].R2;
            case ControlManager.InputCode.Grip:
                return source[controllerID].L1 || source[controllerID].R1 || source[controllerID].Cross;
            case ControlManager.InputCode.Invert:
                return source[controllerID].TouchButton || source[controllerID].L2;
            case ControlManager.InputCode.Menu:
                return source[controllerID].Options;
            case ControlManager.InputCode.RoughnessButtonPlus:
                return source[controllerID].DpadUp;
            case ControlManager.InputCode.RoughnessButtonMinus:
                return source[controllerID].DpadDown;
            case ControlManager.InputCode.SizeButtonPlus:
                return source[controllerID].DpadRight;
            case ControlManager.InputCode.SizeButtonMinus:
                return source[controllerID].DpadLeft;
            default:
                return false;
        }
    }

    public float GetAxis(ControlManager.InputCode axisName, byte controllerID)
    {
       
        return convertAxis(axisName, controllerID, data);
    }

    public bool GetButton(ControlManager.InputCode buttonName, byte controllerID)
    {
        return convertButton(buttonName, controllerID, data);
    }

    public bool GetButtonUp(ControlManager.InputCode buttonName, byte controllerID)
    {
        bool now = convertButton(buttonName, controllerID, data);
        bool then = convertButton(buttonName, controllerID, data_lastFrame);

        if (then == true && now == false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public bool GetButtonDown(ControlManager.InputCode buttonName, byte controllerID)
    {
        bool now = convertButton(buttonName, controllerID, data);
        bool then = convertButton(buttonName, controllerID, data_lastFrame);

        if (then == false && now == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
