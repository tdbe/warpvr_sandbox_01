﻿using UnityEngine;
using System.Collections;

/// <summary>
/// TO IMPLEMENT
/// </summary>
public class PCControlManager : IStandardizedControl
{
    int mouseIntersectionPlaneLayer = 31;
    Transform mouseIntersectionPlane;
    Vector3 mousePosLastTime;

    float panSpeed = 0.1f*60;
    float panSpeedZ = 5*60;

    readonly string _Empty = "";
    readonly string _RoughnessButtonModifier = "RoughnessButtonModifier";
    readonly string _SizeButtonPlus = "SizeButtonPlus";
    readonly string _SizeButtonMinus = "SizeButtonMinus";


    public byte numControllers
    {
        get
        {
            return 1;//assumes only 1 mouse+kb. TODO: TO IMPLEMENT
        }
    }

    public PCControlManager()
    {

        mouseIntersectionPlane = GameObject.Find(LayerMask.LayerToName(mouseIntersectionPlaneLayer)).transform;
        mousePosLastTime = Input.mousePosition;//TODO: assumes 1 mouse
    }


    public void GrabParticleVolume(ref ParticleInteractor.SceneRotationSpeed rotationSpeed, byte controllerID, Transform thisInteractor)
    {
        //TODO: this is the old prototype code, which didn't have parallelization in mind, and has hardcoded stuff. Upgrade.
        //TODO: this assumes only 1 mouse+kb (which is pretty much correct all the time), controllerID not used
        
        //Rotate the particle volume
        rotationSpeed.currentSpeed = rotationSpeed.idleSpeed;
        if ((GetButton(ControlManager.InputCode.Fire, controllerID) || GetButton(ControlManager.InputCode.Invert, controllerID)) && GetButton(ControlManager.InputCode.AltGrip, controllerID))
        {
            rotationSpeed.currentSpeed = rotationSpeed.fullSpeed;
        }
        //if (!UnityEngine.VR.VRSettings.enabled)
        Vector3 angles = new Vector3(GetAxis(ControlManager.InputCode.YAxisMove, controllerID), -GetAxis(ControlManager.InputCode.XAxisMove, controllerID), 0) * Time.deltaTime * rotationSpeed.currentSpeed;
        MainGame.Instance.particleRootPos.transform.RotateAround(MainGame.Instance.particleRootPos.transform.position,Vector3.up, angles.y);
        MainGame.Instance.particleRootPos.transform.RotateAround(MainGame.Instance.particleRootPos.transform.position, Vector3.right, angles.x);


        //pan the particle volume xy wise
        //pan the particle volume
        if (GetButton(ControlManager.InputCode.Grip, controllerID))
        {
            Vector3 diff = Input.mousePosition - mousePosLastTime;
            diff.z = 0;
            //Debug.Log(Vector3.Distance(Input.mousePosition, mousePosLastTime));
            if(Vector3.Distance(Input.mousePosition, mousePosLastTime) < 200)
                MainGame.Instance.particleRootPos.transform.Translate(diff * panSpeed * Time.deltaTime, RenderManager.Instance.mainCam.transform);
        }
        mousePosLastTime = Input.mousePosition;

        //"pan the particle volume" z wise
        if (GetAxis(ControlManager.InputCode.ZAxisMove, controllerID) < 0)
        {
            Vector3 pos = MainGame.Instance.particleRootPos.transform.localPosition;
            pos.z += panSpeedZ * Time.deltaTime;
            MainGame.Instance.particleRootPos.transform.localPosition = pos;
        }
        else
        if (GetAxis(ControlManager.InputCode.ZAxisMove, controllerID) > 0)
        {
            Vector3 pos = MainGame.Instance.particleRootPos.transform.localPosition;
            pos.z -= panSpeedZ * Time.deltaTime;
            MainGame.Instance.particleRootPos.transform.localPosition = pos;
        }
    }

    public void Get3DPos(ref bool onGUI, ref Vector3 pos, ref Quaternion rot, ref Rigidbody rigidb, float moveSensitivity, byte controllerID)
    {
        //Using the mouse to sculpt.
        //No collision on actual particle volume (yet).
        //I just raytrace the mouse to a 2d plane that moves along camera Z.

        //TODO: this assumes only 1 mouse+kb, controllerID not used
        /*
        if (GetAxis(ControlManager.InputCode.ZAxisMove, controllerID) != 0)
        {
            Vector3 ppos = mouseIntersectionPlane.transform.localPosition;
            ppos.z = RenderManager.Instance.mainCam.transform.localPosition.z * 0.33f;
            mouseIntersectionPlane.transform.localPosition = ppos;
        }*/

        Ray ray = RenderManager.Instance.mainCam.ScreenPointToRay(Input.mousePosition);
        int mask = 1 << mouseIntersectionPlaneLayer ;
        RaycastHit info = new RaycastHit();
        if (Physics.Raycast(ray, out info, Mathf.Infinity, mask))
        {
            //8
            //Debug.Log (info.collider.gameObject.layer);

            pos = new Vector3(info.point.x, info.point.y, info.point.z);
        }

        //TODO: no controller rotation for mouse?.
        //rot = Quaternion.identity;
        rot = RenderManager.Instance.mainCam.transform.rotation;
    }

    private string convertAxis(ControlManager.InputCode axisName, byte controllerID)
    {
        switch (axisName)
        {
            
            default:
                return axisName.ToString();
        }
    }

    private string convertButton(ControlManager.InputCode buttonName, byte controllerID)
    {
        switch (buttonName)
        {
            case ControlManager.InputCode.RoughnessButtonPlus:
                if (Input.GetButton(_RoughnessButtonModifier))
                {
                    return _SizeButtonPlus;
                }
                else return _Empty;
            case ControlManager.InputCode.RoughnessButtonMinus:
                if (Input.GetButton(_RoughnessButtonModifier))
                {
                    return _SizeButtonMinus;
                }
                else return _Empty;
            case ControlManager.InputCode.SizeButtonPlus:
                if (!Input.GetButton(_RoughnessButtonModifier))
                {
                    return _SizeButtonPlus;
                }
                else return _Empty;
            case ControlManager.InputCode.SizeButtonMinus:
                if (!Input.GetButton(_RoughnessButtonModifier))
                {
                    return _SizeButtonMinus;
                }
                else return _Empty;
            default:
                return buttonName.ToString();
        }
    }

    public float GetAxis(ControlManager.InputCode axisName, byte controllerID)
    {
        string an = convertAxis(axisName, controllerID);
        if (an.Length > 0)
            return Input.GetAxis(an);
        else
            return 0;
    }

    public bool GetButton(ControlManager.InputCode buttonName, byte controllerID)
    {
        string an = convertButton(buttonName, controllerID);
        if (an.Length > 0)
            return Input.GetButton(an);
        else
            return false;
    }

    public bool GetButtonUp(ControlManager.InputCode buttonName, byte controllerID)
    {
        string an = convertButton(buttonName, controllerID);
        if (an.Length > 0)
            return Input.GetButtonUp(an);
        else
            return false;
    }


    public bool GetButtonDown(ControlManager.InputCode buttonName, byte controllerID)
    {
        string an = convertButton(buttonName, controllerID);
        if (an.Length > 0)
            return Input.GetButtonDown(an);
        else
            return false;
    }
}