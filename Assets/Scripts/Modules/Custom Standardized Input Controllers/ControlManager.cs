using UnityEngine;
using System.Collections;


/// <summary>
/// My Input Manager wraps and standardizes a bunch of different input methods (see Controllers below).
/// It monitors what controllers are available of each class: mouse + kb, DS4 controllers, vive remotes.
/// When you ask for a GetAxis(name, idn), it knows there are x vive controllers, y mice, z ds4's, and fetches the right kind of input from the id-n-th controller.
/// This is done because each controller is a generic Interactor, and I loop through them each frame to send their data to the shaders. And an interactor does not need to have its own controller.
/// </summary>
/// 
//TODO: For future optimization: cache each Input request for each attached controller for the current frame. Right now I'm just calculating them on demand.

public class ControlManager : MonoBehaviourSingleton<ControlManager>, IStandardizedControl
{
   
    public bool onlyUse1stController = false;
#if UNITY_EDITOR
    public bool testInput = false;
    [Header("If Test Input is set to true:")]//TODO: http://stefan-s.net/?p=105
    public byte TestInputIndex = 1;
    public bool TestInputOrientation = false;
    public bool TestInputAim = false;
#endif

    public enum InputCode
    {
        XAxisMove,
        YAxisMove,
        ZAxisMove,
        XAxisAcc,
        YAxisAcc,
        ZAxisAcc,
        XAxisOrientation,
        YAxisOrientation,
        ZAxisOrientation,
        Fire,
        FireAxis,
        FireDemo,
        Invert,
        RoughnessAxis,// used for a trigger or a virtual control like a gui slider
        RoughnessButtonPlus,
        RoughnessButtonMinus,
        SizeAxis,// used for a trigger or a virtual control like a gui slider
        SizeButtonPlus,
        SizeButtonMinus,
        Grip,
        AltGrip,
        Menu,
        Quit
    }
    /*
    public static readonly InputCode i_xAxisAim = InputCode.XAxisMove;
    public static readonly InputCode i_yAxisAim = InputCode.YAxisMove;
    public static readonly InputCode i_zAxisAim = InputCode.ZAxisMove;
    public static readonly InputCode i_xAxisAcc = InputCode.XAxisAcc;
    public static readonly InputCode i_yAxisAcc = InputCode.YAxisAcc;
    public static readonly InputCode i_zAxisAcc = InputCode.ZAxisAcc;
    public static readonly InputCode i_xAxisOrientation = InputCode.XAxisOrientation;
    public static readonly InputCode i_yAxisOrientation = InputCode.YAxisOrientation;
    public static readonly InputCode i_zAxisOrientation = InputCode.ZAxisOrientation;
    public static readonly InputCode i_Fire = InputCode.Fire;
    public static readonly InputCode i_FireAxis = InputCode.FireAxis;
    public static readonly InputCode i_InvertEnergy = InputCode.Invert;
    public static readonly InputCode i_BrushRoughness = InputCode.RoughnessAxis;
    public static readonly InputCode i_BrushSize = InputCode.SizeAxis;
    public static readonly InputCode i_Grab = InputCode.Grip;
    public static readonly InputCode i_MenuButton = InputCode.Menu;*/

    public byte numControllers
    {
        get
        {
//#if UNITY_EDITOR
            if (onlyUse1stController)
                return 1;
            else
//#endif
            return (byte)(_controllers.m_DS4Input.numControllers + _controllers.m_PCInput.numControllers + _controllers.m_ViveInput.numControllers);//nobody has 255 controllers. 
            //return (byte)(controllers.m_PCInput.numControllers);//nobody has 255 controllers. 

        }
    }

    public enum ControllerIDs
    {
        PC,
        DS4,
        Vive
    }

    [SerializeField]
    //SteamVR_TrackedController[] m_steamVRControllerReferences;
    SteamVR_TrackedObject[] m_steamVRControllerReferences;

    public struct Controllers
    {
        public PCControlManager m_PCInput;
        public DS4ControlManager m_DS4Input;
        public ViveControlManager m_ViveInput;
        
        public IStandardizedControl getInput(ControllerIDs id)
        {
            if(id == ControllerIDs.DS4)
            {
                return m_DS4Input;
            }
            else if (id == ControllerIDs.Vive)
            {
                return m_ViveInput;
            }
            else //if (id == ControllerIDs.PC)
            {
                return m_PCInput;
            }
        }
    }
    private Controllers _controllers;
    public Controllers controllers { get { return _controllers; } }

    private int _totalNumberOfJoysticks = 0;
    public int totalNumberOfJoysticks { get { return _totalNumberOfJoysticks; } }

    bool[,] m_uiRaycastThisFrame = new bool[255,2];

    void Start()
    {
        _controllers.m_DS4Input = new DS4ControlManager();
        _controllers.m_ViveInput = new ViveControlManager((SteamVR_TrackedController)m_steamVRControllerReferences);
        _controllers.m_PCInput = new PCControlManager();
        _totalNumberOfJoysticks = Input.GetJoystickNames().Length;
    }

    void Update()
    {

        //if(controllers.m_DS4Input.numControllers > 0)
        _controllers.m_DS4Input.MUpdate();
        _controllers.m_ViveInput.MUpdate();

#if UNITY_EDITOR
        if (testInput)
        {
            for(int i=0; i<13; i++)
            {
                InputCode ic = (InputCode)i;
                if (GetButtonDown(ic, TestInputIndex))
                {
                    Debug.Log("GetButtonDown: " + ic.ToString() + ": " + GetButtonDown(ic, TestInputIndex));
                }
                if (GetButtonUp(ic, TestInputIndex))
                {
                    Debug.Log("GetButtonUp: " + ic.ToString() + ": " + GetButtonUp(ic, TestInputIndex));
                }
                if (Mathf.Abs(GetAxis(ic, TestInputIndex)) > 0.0f)
                {
                    if (ic.ToString().ToLower().Contains("orientation") && TestInputOrientation ||
                        ic.ToString().ToLower().Contains("axisaim") && TestInputAim ||
                        !ic.ToString().ToLower().Contains("orientation") &&
                        !ic.ToString().ToLower().Contains("axisaim"))
                    {
                        Debug.Log("GetAxis: " + ic.ToString() + ": " + GetAxis(ic, TestInputIndex));
                    }

                }
            }
        }
#endif


    }

    /// <summary>
    /// The GUI layer uses Unity's built in input raycasts.
    /// Until further notice, I have to check again myself if the current controller is hitting the gui before I let it Fire.
    /// TODO: fix
    /// </summary>
    /// <returns></returns>
    bool GUIRaycast(byte controllerID)
    {
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (controllerID >= numControllers)
            return false;

        if (m_uiRaycastThisFrame[controllerID, 0])
        {
            return m_uiRaycastThisFrame[controllerID, 1];
        }

        Vector3 position = MainGame.Instance.getInteractorPosition(controllerID);
        position = Camera.main.WorldToScreenPoint(position);
        position.z = 0;

        Ray ray = Camera.main.ScreenPointToRay(position);

        int mask = 1 << 8 | 1 << 5;//button layer
        RaycastHit info = new RaycastHit();
        bool res = false;
        if (Physics.Raycast(ray, out info, Mathf.Infinity, mask))
        {
            res = true;
        }

        m_uiRaycastThisFrame[controllerID, 0] = true;
        m_uiRaycastThisFrame[controllerID, 1] = res;

        return res;
    }

    bool checkHoverPermissions(byte controllerIndex)
    {
        if (
            GUIRaycast(controllerIndex))
        {
            return false;
        }
        else return true;
    }

    bool checkPermissions(InputCode inputName, byte controllerIndex)
    {
        if (
            (inputName == InputCode.Fire || inputName == InputCode.FireAxis || inputName == InputCode.FireDemo)
            && GUIRaycast(controllerIndex))
        {
            return false;
        }
        else return true;
    }

    void LateUpdate()
    {
        if (_controllers.m_DS4Input.numControllers > 0)
            _controllers.m_DS4Input.m_LateUpdate();

        for(int i=0; i<255; i++)
        {
            m_uiRaycastThisFrame[i, 0] = false;
            //uiRaycastThisFrame[i, 1] = false;
        }
        
    }

    public IStandardizedControl getControllerFromIndex(ref byte index)
    {
   
        int num1V = _controllers.m_ViveInput.numControllers;
        int num2P = num1V + _controllers.m_PCInput.numControllers;
        int num3D = num2P + _controllers.m_DS4Input.numControllers;
        if (index < num1V)
        {
            return _controllers.m_ViveInput;
        }
        else if (index < num2P)
        {
            index = (byte)(index - num1V);
            return _controllers.m_PCInput;
        }
        else if (index < num3D)
        {
            index = (byte)(index - num2P);
            return _controllers.m_DS4Input;
        }
        else
        {
            //Debug.LogError("Controller Global Index out or range! Total number of controllers is "+ num3D+". You tried index: "+ index);
            return null;
        }

    }

    /// <summary>
    /// When you want -one- interactor to grab and move or rotate the particle volume, this handles that.
    /// </summary>
    /// <param name="rotationSpeed"></param>
    /// <param name="controllerIndex"></param>
    public void GrabParticleVolume(ref ParticleInteractor.SceneRotationSpeed rotationSpeed, byte controllerIndex, Transform thisInteractor)
    {
        IStandardizedControl isc = getControllerFromIndex(ref controllerIndex);
        if (isc == null) return;
        byte controllerspaceIndex = controllerIndex;
        isc.GrabParticleVolume(ref rotationSpeed, controllerspaceIndex, thisInteractor);
    }

    /// <summary>
    /// This "polls the motion controller's 3D position". Even if it's a a ds4 or a mouse + kb.
    /// The idea is to treat everything like a 3D motion-tracked controller. 
    /// Since I don't know yet what controllers I'll use, it takes both rigidbody and pos & rot. (ps4 motion control uses force, the vive plugin gives me the pos&rot, mouse+kb = pos only etc)
    /// </summary>
    public void Get3DPos(ref bool onGUI, ref Vector3 pos, ref Quaternion rot, ref Rigidbody rigidb, float moveSensitivity, byte controllerIndex)
    {
        onGUI = checkHoverPermissions(controllerIndex);
        IStandardizedControl isc = getControllerFromIndex(ref controllerIndex);
        if (isc == null) return;
        byte controllerspaceIndex = controllerIndex;
        isc.Get3DPos(ref onGUI, ref pos, ref rot, ref rigidb, moveSensitivity, controllerspaceIndex);
    }

    /// <summary>
    /// Returns the value of the specified axis, value is fetched from all connected controller types for this hand.
    /// For polling from individual controllers, refer to the InputManager.Controllers struct.
    /// </summary>
    /// <param name="axisName"></param>
    /// <returns></returns>
    public float GetAxis(InputCode axisName, byte controllerIndex)
    {
        bool permission = checkPermissions(axisName, controllerIndex);
        /*
        float val = 0f;
        val += Controllers.DS4Input.GetAxis(axisName, hand);
        val += Controllers.ViveInput.GetAxis(axisName, hand);
        val += Controllers.PCInput.GetAxis(axisName, hand);
        return val;
        */
        IStandardizedControl isc = getControllerFromIndex(ref controllerIndex);
        if (isc == null || !permission) return 0;
        return isc.GetAxis(axisName, controllerIndex);
    }

    /// <summary>
    /// Returns the value of the specified button, value is fetched from all connected controllers.
    /// For polling from individual controllers, refer to the InputManager.Controllers struct.
    /// </summary>
    /// <param name="buttonName"></param>
    /// <returns></returns>
    public bool GetButton(InputCode buttonName, byte controllerIndex)
    {
        bool permission = checkPermissions(buttonName, controllerIndex);
        /*
        bool val =
            Controllers.DS4Input.GetButton(buttonName, hand) ||
            Controllers.ViveInput.GetButton(buttonName, hand) ||
            Controllers.PCInput.GetButton(buttonName, hand);
        return val;*/
        /*
        int ci = controllerIndex;
        bool r = getControllerFromIndex(ref controllerIndex).GetButton(buttonName, controllerIndex);
        if (buttonName == InputCode.Fire && r == true)
        {
            Debug.Log("fire pressed on " + ci);
        }
        return r;
        */
        IStandardizedControl isc = getControllerFromIndex(ref controllerIndex);
        if (isc == null || !permission) return false;
        return isc.GetButton(buttonName, controllerIndex);
    }

    /// <summary>
    /// Returns the value of the specified button, value is fetched from all connected controllers.
    /// For polling from individual controllers, refer to the InputManager.Controllers struct.
    /// </summary>
    /// <param name="buttonName"></param>
    /// <returns></returns>
    public bool GetButtonUp(InputCode buttonName, byte controllerIndex)
    {
        bool permission = checkPermissions(buttonName, controllerIndex);
        /*
        bool val = 
            Controllers.DS4Input.GetButtonUp(buttonName, hand) ||
            Controllers.ViveInput.GetButtonUp(buttonName, hand) ||
            Controllers.PCInput.GetButtonUp(buttonName, hand);
        return val;*/
        IStandardizedControl isc = getControllerFromIndex(ref controllerIndex);
        if (isc == null || !permission) return false;
        return isc.GetButtonUp(buttonName, controllerIndex);
    }

    /// <summary>
    /// Returns the value of the specified button, value is fetched from all connected controllers.
    /// For polling from individual controllers, refer to the InputManager.Controllers struct.
    /// </summary>
    /// <param name="buttonName"></param>
    /// <returns></returns>
    public bool GetButtonDown(InputCode buttonName, byte controllerIndex)
    {
        bool permission = checkPermissions(buttonName, controllerIndex);
        /*
        bool val =
            Controllers.DS4Input.GetButtonDown(buttonName, hand) ||
            Controllers.ViveInput.GetButtonDown(buttonName, hand) ||
            Controllers.PCInput.GetButtonDown(buttonName, hand);
        return val;*/
        
        IStandardizedControl isc = getControllerFromIndex(ref controllerIndex);
        if (isc == null || !permission) return false;
        byte controllerspaceIndex = controllerIndex;
        return isc.GetButtonDown(buttonName, controllerspaceIndex);
    }



}
