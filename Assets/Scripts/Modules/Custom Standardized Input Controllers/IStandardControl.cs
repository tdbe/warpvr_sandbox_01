﻿using UnityEngine;

public interface IStandardizedControl {

    byte numControllers { get; }

    void GrabParticleVolume(ref ParticleInteractor.SceneRotationSpeed rotationSpeed, byte controllerIndex, Transform thisInteractor);

    //void Get3DPos(ref Vector3 pos, ref Quaternion rot, ref Rigidbody rigidb, float moveSensitivity, byte controllerID);
    void Get3DPos(ref bool onGUI, ref Vector3 pos, ref Quaternion rot, ref Rigidbody rigidb, float moveSensitivity, byte controllerID);


    float GetAxis(ControlManager.InputCode axisName, byte controllerID);

    bool GetButton(ControlManager.InputCode axisName, byte controllerID);

    bool GetButtonUp(ControlManager.InputCode buttonName, byte controllerID);

    bool GetButtonDown(ControlManager.InputCode buttonName, byte controllerID);

}
