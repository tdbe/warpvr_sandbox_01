﻿using UnityEngine;
using System.Collections;

public class ShowPanels : MonoBehaviour {

    [SerializeField]
	GameObject optionsPanel;                            //Store a reference to the Game Object OptionsPanel 
    [SerializeField]
    GameObject mainMenu_btn;
    //private GameObject optionsTint;						//Store a reference to the Game Object OptionsTint 
    //private GameObject menuPanel;							//Store a reference to the Game Object MenuPanel 
    //private GameObject pausePanel;							//Store a reference to the Game Object PausePanel 
    [SerializeField]
    GameObject physicsPanel;
    [SerializeField]
    GameObject brushSettingsPanel;
    [SerializeField]
    GameObject gameOptions;
    [SerializeField]
    GameObject about;
    [SerializeField]
    Transform inputTabParent;
    [SerializeField]
    float tabSetActiveDelay = 0.2f;

    PlayMusic playMusic;

    void Start()
    {
        playMusic = GetComponent<PlayMusic>();
    }

    void Update()
    {
        for(byte i = 0; i < MainGame.Instance.particleInteractorCount; i++)
        {
            if (ControlManager.Instance.GetButtonDown(ControlManager.InputCode.Menu, i))
            {
                ToggleOptionsPanel();
            }
            else if (ControlManager.Instance.GetButtonDown(ControlManager.InputCode.Quit, i))
            {
                MainGame.Instance.quit();
            }
                
        }
        
        
    }

    public void ShowInputTab(int index)
    {
        bool on = true;
        int i = 0;
        Transform found = null;
        foreach (Transform tran in inputTabParent)
        {
            if(index == i)
            {
                found = tran;
                StartCoroutine(doAfterTime(()=> { found.gameObject.SetActive(on); }, tabSetActiveDelay));
                
            }
            else
            {
                tran.gameObject.SetActive(!on);
            }
            i++;
        }

    }

    public void refreshCanvases()
    {
        Canvas.ForceUpdateCanvases();

    }

    IEnumerator doAfterTime(System.Action action, float duration)
    {
        yield return new WaitForSeconds(duration);
        action();
    }

    public void ShowPhysicsSettingsPanel()
    {
        bool on = true;
        physicsPanel.SetActive(on);
        brushSettingsPanel.SetActive(!on);
        gameOptions.SetActive(!on);
        about.SetActive(!on);
    }

    public void ShowBrushSettingsPanel()
    {
        bool on = true;
        physicsPanel.SetActive(!on);
        brushSettingsPanel.SetActive(on);
        ShowInputTab(-1);

        gameOptions.SetActive(!on);
        about.SetActive(!on);
    }

    public void ShowGameOptionsPanel()
    {
        bool on = true;
        physicsPanel.SetActive(!on);
        brushSettingsPanel.SetActive(!on);
        gameOptions.SetActive(on);
        about.SetActive(!on);
    }

    public void ShowAboutPanel()
    {
        bool on = true;
        physicsPanel.SetActive(!on);
        brushSettingsPanel.SetActive(!on);
        gameOptions.SetActive(!on);
        about.SetActive(on);
    }

    public void ToggleOptionsPanel()
    {
        if (optionsPanel.activeInHierarchy)
        {
            HideOptionsPanel();
        }
        else
        {
            ShowOptionsPanel();
        }
    }

    //Call this function to activate and display the Options panel during the main menu
    public void ShowOptionsPanel()
	{

        mainMenu_btn.SetActive(false);
        optionsPanel.SetActive(true);
        //optionsTint.SetActive(true);
        playMusic.checkPauseMenuMusic();
    }

    //Call this function to deactivate and hide the Options panel during the main menu
    public void HideOptionsPanel()
	{
        mainMenu_btn.SetActive(true);
        optionsPanel.SetActive(false);
        //optionsTint.SetActive(false);
        playMusic.checkPauseMenuMusic();
    }
    
	//Call this function to activate and display the main menu panel during the main menu
	public void ShowMenu()
	{
        //menuPanel.SetActive (true);
#if UNITY_EDITOR
        Debug.LogWarning("Show Main Menu");
#endif
    }

	//Call this function to deactivate and hide the main menu panel during the main menu
	public void HideMenu()
	{
        //menuPanel.SetActive (false);
#if UNITY_EDITOR
        Debug.LogWarning("Close Main Menu");
#endif
    }

    //Call this function to activate and display the Pause panel during game play
    public void ShowPausePanel()
	{
        //pausePanel.SetActive (true);
        //optionsTint.SetActive(true);
#if UNITY_EDITOR
        Debug.LogWarning("not needed");
#endif
    }

    //Call this function to deactivate and hide the Pause panel during game play
    public void HidePausePanel()
	{
        //pausePanel.SetActive (false);
        //optionsTint.SetActive(false);
#if UNITY_EDITOR
        Debug.LogWarning("Close Pause Menu");
#endif
    }
}
