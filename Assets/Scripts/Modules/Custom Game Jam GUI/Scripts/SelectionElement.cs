﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class SelectionElement : MonoBehaviour {

    public SelectionTracker parent;
    public int depth = 0;

    public Color m_colorSelected = new Color(0.6941f, 0.9607f, 0.9607f, 1);
    public Color m_colorNormal = Color.white;
    public Color m_colorDisabled = new Color(0.7843f, 0.7843f, 0.7843f, .5f);

    Button m_button;


	// Use this for initialization
	void Awake () {
        m_button = GetComponent<Button>();
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    void clearSiblingsOfSameDepthOrBelow()
    {

        for (int i = 0; i < parent.guiSelectionPath.Count; i++)
        {


            for (int d = depth; d < parent.maxTreeDepth; d++)
            {

                if ( i < parent.guiSelectionPath.Count && parent.guiSelectionPath[i].depth == d && parent.guiSelectionPath[i].m_button != null)
                {
                    setButtonNormalColor(parent.guiSelectionPath[i].m_button, m_colorNormal);
                    setButtonDisabledColor(parent.guiSelectionPath[i].m_button, m_colorDisabled);
                    parent.guiSelectionPath[i].m_button.interactable = true;
                    parent.guiSelectionPath.RemoveAt(i);
                    i = i > 0 ? i - 1 : 0;
                }
            }
        }
    }

    void setButtonNormalColor(Button button, Color color)
    {
        ColorBlock cb = button.colors;
        cb.normalColor = color;
        button.colors = cb;
    }

    void setButtonDisabledColor(Button button, Color color)
    {
        ColorBlock cb = button.colors;
        cb.disabledColor = color;
        button.colors = cb;
    }

    public void OnSelected()
    {
        if (m_button == null)
        {
            return;
        }
        clearSiblingsOfSameDepthOrBelow();
        parent.guiSelectionPath.Add(this);
        setButtonNormalColor(m_button, m_colorSelected);
        setButtonDisabledColor(m_button, m_colorSelected);
        m_button.interactable = false;
    }
}
