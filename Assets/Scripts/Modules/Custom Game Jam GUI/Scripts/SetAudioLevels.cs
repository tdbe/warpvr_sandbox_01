﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetAudioLevels : MonoBehaviour, IReceiveActiveEvent
{

	public AudioMixer mainMixer;					//Used to hold a reference to the AudioMixer mainMixer
    public float defaultMusicLvlValue = -40;
    public float defaultSfxLvlValue = -40;
    public Slider musicLvlSlider;
    public Slider sfxLvlSlider;

    void Start()
    {

        SetMusicLevel(defaultMusicLvlValue);
        SetSfxLevel(defaultSfxLvlValue);

    }

    public void ReceiveAwakeEvent(GameObject from, bool active)
    {
        Slider slider = from.GetComponent<Slider>();
        if (slider)
        {
            if (slider.Equals(musicLvlSlider))
            {
                musicLvlSlider.value = defaultMusicLvlValue;
            }
            else if (slider.Equals(sfxLvlSlider))
            {
                sfxLvlSlider.value = defaultSfxLvlValue;
            }
        }
        
    }

    public void ReceiveStartEvent(GameObject from, bool active)
    {

    }

    //Call this function and pass in the float parameter musicLvl to set the volume of the AudioMixerGroup Music in mainMixer
    public void SetMusicLevel(float musicLvl)
	{
		mainMixer.SetFloat("musicVol", musicLvl);
	}

	//Call this function and pass in the float parameter sfxLevel to set the volume of the AudioMixerGroup SoundFx in mainMixer
	public void SetSfxLevel(float sfxLevel)
	{
		mainMixer.SetFloat("sfxVol", sfxLevel);
	}
}
