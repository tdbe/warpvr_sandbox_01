﻿using UnityEngine;
using System.Collections.Generic;

/*
For now, this experiment is under the following Creative Commons license: Attribution - NonCommercial - NoDerivs https://creativecommons.org/licenses/by-nc-nd/4.0/ 
Basically, if you are senpai, let's contribute to the same project.
*/

/*
This version is mostly a research prototype, so the way that MainGame, RenderManager and ParticleInteractor etc. are wired together may be prototypish.
*/

/*
    This app has a cube of particles with a physics preset (ie how their compute shader reacts to velocity changes)
    And up to 256 generic Interactor volume brushes, which possess an input controller (of various kinds), or are controlled only via GUI.
    The Interactors each apply preconfigured and emergent forces in a certain way, and are accumulated in the Compute Shader, and the 
    particle volume reacts to the forces according to the phys preset.  
*/

public class MainGame : MonoBehaviourSingleton<MainGame> {


    public int particleInteractorCount
    {
        get { return particleInteractors.Count; }
    }
    public int virtualParticleInteractorCount
    {
        get { return particleInteractorsVirtual.Count; }
    }

    //public Transform rotationPivot;
    public Transform particleRootPos;
    public Transform particleRootPosParent;

    [SerializeField]
    Transform m_particleInteractorsParent;
    [SerializeField]
    Transform m_dummyParticleInteractor;
    [SerializeField]
    GameObject m_particleInteractorPrefab;
    [SerializeField]
    public List<ParticleInteractor> particleInteractorsVirtual = new List<ParticleInteractor>();
    [SerializeField]
    public List<ParticleInteractor> particleInteractors = new List<ParticleInteractor>();


    [System.Serializable]
    public class ParticlePhysicsProperties
    {
        public float drag = 0.0f;
        public float verletElasticity = 0.01f;
        public float interactorScatterControl = 3;
    }

    public static class CSKeywords
    {
        public static readonly string _ParticleBuffer = "particleBuffer";
        public static readonly string _Flush = "flush";
        public static readonly string _Drag = "drag";
        public static readonly string _VerletElasticity = "verletElasticity";
        public static readonly string _InteractorCount = "interactorCount";
        public static readonly string _InteractorData = "interactorData";
        
    }

    public static class ShaderKeywords
    {
        public static readonly string _ParticleBuffer = "particleBuffer";
        public static readonly string _TargetRotationMatrix = "_TargetRotationMatrix";
        public static readonly string _ColorRadiusScale = "_colorRadiusScale";
        public static readonly string _QuadVerts = "quad_verts";
        public static readonly string _SpeedColorOne = "_SpeedColorOne";
        public static readonly string _SpeedColorTwo = "_SpeedColorTwo";
        

    }

    [System.Serializable]
    public class PhysicsSettings
    {
        public enum CSNames
        {
            StrobeString,
            AAAParticles,
            JigglePhysics
        }

        [System.Serializable]
        public class ComputePhysicsEntity
        {
            public ParticlePhysicsProperties particlePhysicsProperties;
            public CSNames name;
            public ComputeShader cs;
        }
        public ComputePhysicsEntity[] computeEntities;

        public ComputePhysicsEntity current
        {
            get
            {
                if (m_currentCPE == null || m_currentCPE.cs == null)
                {
                    m_currentCPE = computeEntities[(int)MainGame.Instance.defaultSetting];
                }
                return m_currentCPE;
            }
            set
            {
                //_currentCPE = value;
            }

        }
        [SerializeField]
        ComputePhysicsEntity m_currentCPE;

        public ComputePhysicsEntity getByName(CSNames name)
        {
            foreach (ComputePhysicsEntity cse in computeEntities)
            {
                if (cse.name == name)
                {
                    return cse;
                }
            }
            return null;
        }


        public void setCurrentSettingsTo(byte arrayEntry)
        {

            m_currentCPE = computeEntities[arrayEntry];
        }
    }
    public PhysicsSettings.CSNames defaultSetting;
    public PhysicsSettings physicsSettings; //currently active compute settings: volume properties + compute shader

    public int virtualInteractorNumber = 1;

    bool m_interactorsChanged = false;

    // Use this for initialization
    void Start () {
        Application.targetFrameRate = 90;

        Cursor.visible = false;
        for(int i=0; i< virtualInteractorNumber; i++)
        {
            createInteractor(true,"_Virtual_"+i);
        }

    }

    void Update()
    {

        //for all controllers of all types
        for (byte i = 0; i < ControlManager.Instance.numControllers; i++)
        {
            //My input manager module is the one who checks for controller changes in fixedUpdate (ie new controllers plugged). 
            //So I just poll the input data here and run the Interactors, and create new generic Interactors if necessary.
            if (i >= particleInteractors.Count)
            {
                createInteractor(false, "_"+i);
                m_interactorsChanged = true;
            }
            //TODO: Maintain proper controller ids across all controller types and drivers.

            //Check if we dropped any controllers
            else if (particleInteractors.Count > ControlManager.Instance.numControllers)
            {
                int diff = particleInteractors.Count - ControlManager.Instance.numControllers;
                for (int j = 0; j < diff; j++)
                {
                    Destroy(particleInteractors[j].gameObject);
                }
                particleInteractors.RemoveRange(particleInteractors.Count - 1 - diff, diff);
                m_interactorsChanged = true;
            }
            else // All Controllers have an Interactor and we can run them
            {
                particleInteractors[i].Interact(i);//interactors gonna interact
            }

        }

        //This is for demo purposes. You can create controllerless Virtual Interactor brushes through the interface.
        for (byte i = 0; i < particleInteractorsVirtual.Count; i++)
        {
            particleInteractorsVirtual[i].Interact((byte)(particleInteractors.Count+i));//controllerless interactors gonna interact
        }
    }

    void LateUpdate()
    {
        if (m_interactorsChanged)
        {
            m_interactorsChanged = false;
            AppGUI.Instance.refreshUIControllerList();
        }
    }
    
    //Interactors have their own settings, but the Particle Volume itself has Physics Settings
    //Basically how the volume reacts to forces. There are multiple presets in the Inspector.
    public void OnPhysicsSettingsChanged(byte index)
    {
#if UNITY_EDITOR
        Debug.Log("Set Physics preset to: " + index);
#endif
        physicsSettings.setCurrentSettingsTo((byte)Mathf.Clamp(index, 0, 255));
        RenderManager.Instance.flushParticleVolumeCSValues();
        flushValuesOfAllInteractors();
    }

    public void OnApplicationResetRequest()
    {
        RenderManager.Instance.resetParticleBuffer();
        RenderManager.Instance.flushParticleVolumeCSValues();
        flushValuesOfAllInteractors();
    }

    void flushValuesOfAllInteractors()
    {
        for (int i = 0; i < particleInteractors.Count; i++)
        {
            particleInteractors[i].OnPhysicsSettingsChanged();
        }
        for (int i = 0; i < particleInteractorsVirtual.Count; i++)
        {
            particleInteractorsVirtual[i].OnPhysicsSettingsChanged();
        }
    }

    public void appendVirtualInteractor()
    {
        createInteractor(true, "_Virtual_" + particleInteractorsVirtual.Count);
        m_interactorsChanged = true;
    }

    public void deleteLastVirtualInteractor()
    {
        if(particleInteractorsVirtual.Count == 0)
            return;

        int index = particleInteractorsVirtual.Count - 1;
        particleInteractorsVirtual[index].deleteSelf();
        particleInteractorsVirtual.RemoveAt(index);
        m_interactorsChanged = true;
    }

    void createInteractor(bool isVirtual, string num)
    {
        GameObject pI_go = (GameObject)Instantiate(
                            m_particleInteractorPrefab,
                            m_dummyParticleInteractor.position,
                            //dummyParticleInteractor.rotation);
                            //RenderManager.Instance.mainCam.transform.rotation);
                            Quaternion.identity);
      
        pI_go.transform.parent = m_particleInteractorsParent;
        ParticleInteractor pI = pI_go.GetComponent<ParticleInteractor>();
        pI_go.name += num;
        if (isVirtual)
        {
            pI.isDemoInteractor = true;
            particleInteractorsVirtual.Add(pI);
        }
        else
        {
            particleInteractors.Add(pI);
        }
            
    }

    public Vector3 getInteractorPosition(byte index)
    {
        return particleInteractors[index].transform.position;
    }

    public void openURL(string url)
    {
        Application.OpenURL(url);
    }

    public void OnGammaChanged(float val)
    {
        RenderManager.Instance.scaleBgColor(val);
    }

    public void quit()
    {
        RenderManager.Instance.OnDestroy();
#if UNITY_STANDALONE

        Application.Quit();
#endif

#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }


}
