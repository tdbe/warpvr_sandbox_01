﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


/// <summary>
/// ParticleInteractor represents one tool in the scene (e.g. a vive controller, a mouse, a ds4 pad)
/// It acts as a generic tool.
/// </summary>
public partial class ParticleInteractor : MonoBehaviour {

    /// <summary>
    /// This manages the local GUI, like for example the brush size sprite and sliders, or if the interactor has a gui or HUD right above it, as opposed to the global app interface.
    /// </summary>
    public class ParticleInteractorGUI
    {
        ParticleInteractor outer;
        Material brushIconMat;
        internal Slider brushSizeSlider;
        float currentBSSVal;
        internal Slider scatterControlSlider;
        float currentSCSVal;

        internal Vector3 initialPosition;


        public ParticleInteractorGUI(ParticleInteractor outer)
        {
            this.outer = outer;
        }

        public void Start()
        {
            scaleInteractorTip();
            
        }

        public void Update()
        {
            //scaleInteractorTip();
        }

        public void LateUpdate()
        {
            
        }

        /// <summary>
        /// In case I want to have the tip of the controller change size based on the brush size
        /// </summary>
        public void scaleInteractorTip()
        {
            //outer.transform.localScale = Vector3.one * outer.m_sculptBrush.sculptStrength * outer.m_sculptBrush.brushTipScale;
            outer.transform.localScale = Vector3.one * outer.m_sculptBrush.brushTipScale;
            //outer.transform.localScale = Vector3.one;
        }


        public void OnBrushSizeSliderChanged(float val)
        {
            if (currentBSSVal == val)
                return;
            outer.m_sculptBrush.sculptStrength = val;
        }

        public void OnScatterControlSliderChanged(float val)
        {
            if (currentSCSVal == val)
                return;
            outer.distanceField.distanceFieldRadius = val;
            setBrushIconMat(val);
        }

        public void OnMoveXAxisSliderChanged(float val)
        {
            Vector3 pos = outer.transform.localPosition;
            pos.x = initialPosition.x + val;
            outer.transform.localPosition = pos;
        }

        public void OnMoveYAxisSliderChanged(float val)
        {
            Vector3 pos = outer.transform.localPosition;
            pos.y = initialPosition.y + val;
            outer.transform.localPosition = pos;
        }

        public void OnMoveZAxisSliderChanged(float val)
        {
            Vector3 pos = outer.transform.localPosition;
            pos.z = initialPosition.z + val;
            outer.transform.localPosition = pos;
        }

        public void linkBrushIconMaterial(Material mat)
        {
            brushIconMat = mat;
            setBrushIconMat(outer.distanceField.distanceFieldRadius);
        }

        public void setBrushIconMat(float val)
        {
            brushIconMat.SetFloat(
                "_Contrast",
                Utils.RemapInterval(
                    outer.distanceField.minRadius,
                    outer.distanceField.maxRadius,
                    1,//value from shader, should clean this up
                    5,//likewise
                    val
                    )
               );
        }

        internal void setBrushSizeGUI(float val)
        {
            brushSizeSlider.value = currentBSSVal = val;
        }

        internal void setScatterControlGUI(float val)
        {
            scatterControlSlider.value = currentSCSVal = val;
            setBrushIconMat(val);
        }


    }
    
}
