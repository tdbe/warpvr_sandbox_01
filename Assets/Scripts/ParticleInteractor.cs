﻿using UnityEngine;
using System.Collections;

/// <summary>
/// ParticleInteractor represents one generic tool in the scene (e.g. a vive controller, a mouse, a ds4 controller)
/// </summary>
public partial class ParticleInteractor : MonoBehaviour {

    [HideInInspector]
    public ParticleInteractorGUI piGUI;

    public bool isDemoInteractor = false;

    [SerializeField]
    float m_moveSensitivity = 10000;
    [System.Serializable]
    public class SculptBrush
    {
        public Vector3 position;
        public Color brushColor;
        public float sculptStrength
        {
            get { return m_sculptStrength; }
            set
            {
                if(value < min)
                {
                    m_sculptStrength = min;
                }
                else if(value > max)
                {
                    m_sculptStrength = max;
                }
                else
                {
                    m_sculptStrength = value;
                }
            }
        }
        public float min = 0;
        public float max = 200;
        public float defaultStr;
        public float increaseValueSpeed = 10;
        public float brushTipScale = 1;
        public float brushMoveSpeed = 0;
        [SerializeField]
        private float m_sculptStrength = 7;

        public void Init()
        {
            defaultStr = sculptStrength;
        }
    }
    [SerializeField]
    SculptBrush m_sculptBrush;
    [System.Serializable]
    class DistanceField
    {
        public float distanceFieldRadius
        {
            get { return m_currentRadius; }
            set
            {
                if (value < minRadius)
                {
                    m_currentRadius = minRadius;
                }
                else if (value > maxRadius)
                {
                    m_currentRadius = maxRadius;
                }
                else
                {
                    m_currentRadius = value;
                }
            }
        }
        public float minRadius = 0.0f;
        public float maxRadius = 3.0f;
        public int dfSign = 1;//the sign of the distance field. Whether to shrink or expand
        public float increaseValueSpeed = 1;
        [SerializeField]
        private float m_currentRadius = 0.33f;

        public void Init()
        {
        }
     }
    [SerializeField]
    DistanceField distanceField;

    [System.Serializable]
    class BrushShapeWarps
    {
        public float noiseSeedMult = 3000;
        public float sizeNoiseFrequency = 80;
        public Vector2 sizeNoiseMinMax = new Vector2(0.008f, 0.08f);
        public Vector2 mouseSpeedIntervalForExplosion = new Vector2(100, 500);
        public float explosionNoiseChance = .85f;
        public float explosionNoiseFrequency = 0.1f;
        public Vector2 explosionNoiseMinMax = new Vector2(0.04f, 0.4f);
        public float posShakeScale = 0.1f;
        [HideInInspector]
        public float sizeNoise;
        [HideInInspector]
        public float explosionNoise;
        public void Init()
        {
        }
    }
    [SerializeField]
    BrushShapeWarps brushWarps;

    [System.Serializable]
    public class SceneRotationSpeed
    {
        public float currentSpeed = 0.15f;
        public float idleSpeed = 0.15f;
        public float fullSpeed = 100;
    }
    [SerializeField]
    SceneRotationSpeed sceneRotationSpeed = new SceneRotationSpeed();

    public float positionSliderMin;
    public float positionSliderMax;

    new Rigidbody rigidbody;
    byte m_globalControllerId;
    byte m_globalGPUInteractorId;
    Vector3 m_prevPos;
    Vector3 m_noiseSeed;


    void Awake()
    {
        piGUI = new ParticleInteractorGUI(this);
        m_sculptBrush.Init();
        distanceField.Init();
        brushWarps.Init();
    }

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        piGUI.Start();
        StartCoroutine(doActionNextFrame(OnPhysicsSettingsChanged));

        if (isDemoInteractor)//quick hack to have controllerless interactors in the scene that fire on InputCode.FireDemo
        {
            //MeshRenderer[] mra = GetComponentsInChildren<MeshRenderer>();
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(3).gameObject.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
        piGUI.Update();
        /*
        //quick hack to have controllerless interactors in the scene that fire on InputCode.FireDemo
        if (isDemoInteractor)
        {
            Interact(ControlManager.Instance.numControllers);
        }*/
	}

    void LateUpdate()
    {
        piGUI.LateUpdate();
    }

    public void Interact(byte globalControllerId)
    {

        //Note: 
        //      This is only called if/when controller active in main game and contains only volume interaction code
        
        m_globalGPUInteractorId = m_globalControllerId = globalControllerId;

        m_noiseSeed = Vector3.one * m_globalControllerId * brushWarps.noiseSeedMult;

        moveControlPoint();
        panAndRotateScene(ref sceneRotationSpeed);

        setBrushSize();
        setBrushRoughness();
        setBrushSign();


        warpTheBrush();


        setComputeFloats();
        setRenderFloats();

    }

    /// <summary>
    /// Adds noise based size alterations and speed based + chance based explosions.
    /// </summary>
    void warpTheBrush()
    {
 
        float bmsS = Mathf.Clamp(m_sculptBrush.brushMoveSpeed - brushWarps.mouseSpeedIntervalForExplosion.x, 0, brushWarps.mouseSpeedIntervalForExplosion.y);
        float bmsE = Utils.RemapInterval(0, brushWarps.mouseSpeedIntervalForExplosion.y, 1, 1 - brushWarps.explosionNoiseChance, bmsS);
        bmsS = Utils.RemapInterval(0, brushWarps.mouseSpeedIntervalForExplosion.y, 1, 1.25f, bmsS);

        brushWarps.sizeNoise = Simplex.Noise.Generate(m_noiseSeed.x + Time.time * brushWarps.sizeNoiseFrequency) * 250;
        brushWarps.sizeNoise = Utils.RemapInterval(0, 255, brushWarps.sizeNoiseMinMax.x, brushWarps.sizeNoiseMinMax.y, brushWarps.sizeNoise) * bmsS;

        if (brushWarps.sizeNoise > brushWarps.sizeNoiseMinMax.y * bmsE)
        {
            brushWarps.explosionNoise = Simplex.Noise.Generate(m_noiseSeed.x + Time.time * brushWarps.explosionNoiseFrequency);
            brushWarps.explosionNoise = Utils.RemapInterval(0, 1, brushWarps.explosionNoiseMinMax.x, brushWarps.explosionNoiseMinMax.y, brushWarps.explosionNoise);
        }
        else
        {
            brushWarps.explosionNoise = 0;
        }
        
    }
    
    void panAndRotateScene(ref SceneRotationSpeed rotationSpeed)
    {
        //TODO: Also implement pan and rotate using any 2+ interactors
        
        //This is pan and rotate using 1 interactor (1 hand)
        ControlManager.Instance.GrabParticleVolume(ref rotationSpeed, m_globalControllerId, transform);
    }

    /// <summary>
    /// adds values to a buffer in the RenderManager, which will be set at the end of each frame, after all info from all Interactors has been collected.
    /// </summary>
    void setRenderFloats()
    {

        //RenderManager.Instance.renderFloats._ColorRadiusScale.array[m_globalGPUInteractorId] = renderColorRadiusScale;
        //RenderManager.Instance.renderFloats._SpeedColor.array[m_globalGPUInteractorId] = sculptBrush.brushColor;
    }

    /// <summary>
    /// adds values to a buffer in the RenderManager, which will be set at the end of each frame, after all info from all Interactors has been collected.
    /// </summary>
    void setComputeFloats()
    {
        
        RenderManager.Instance.interactorData[m_globalGPUInteractorId].target = RenderManager.Instance.particleVolumeTRS.inverse.MultiplyPoint(m_sculptBrush.position);
        RenderManager.Instance.interactorData[m_globalGPUInteractorId].targetStrengh = m_sculptBrush.sculptStrength * distanceField.dfSign*10;
        RenderManager.Instance.interactorData[m_globalGPUInteractorId].sprayOrbRadius = distanceField.distanceFieldRadius;//TODO: this should only go bw 0.0 and 1.0
        RenderManager.Instance.interactorData[m_globalGPUInteractorId].snoise = brushWarps.sizeNoise;
        RenderManager.Instance.interactorData[m_globalGPUInteractorId].enoise = brushWarps.explosionNoise;
        

        //In case you were looking for the code that actually starts sculpting, here it is..                                                                                     
        if (isSculptingCheck())
        {
            RenderManager.Instance.interactorData[m_globalGPUInteractorId].isSculpting = 1;
        }
        else
        {
            //Debug.Log("false");
            RenderManager.Instance.interactorData[m_globalGPUInteractorId].isSculpting = 0;
        }

        //TODO: incorporate controller rotation
    }

    void setBrushSign()
    {
        if (!ControlManager.Instance.GetButtonDown(ControlManager.InputCode.Invert, m_globalControllerId))
        {
            return;
        }

        distanceField.dfSign *= -1;

        if (distanceField.dfSign > 0)
        {
            m_sculptBrush.brushColor = RenderManager.Instance.material.GetColor(MainGame.ShaderKeywords._SpeedColorOne);
            //col = new Color(Color.red.r/2, Color.red.g/2, Color.red.b/2, col.a);
        }
        else
        {
            m_sculptBrush.brushColor = RenderManager.Instance.material.GetColor(MainGame.ShaderKeywords._SpeedColorTwo);
        }
    }

    void setBrushSize()
    {
        //TODO: To implelent Size Axis version:
        //ControlManager.Instance.GetAxis(ControlManager.InputCode.SizeAxis, m_globalControllerIndex)

        bool sizePlus = ControlManager.Instance.GetButton(ControlManager.InputCode.SizeButtonPlus, m_globalControllerId);
        bool sizeMinus = ControlManager.Instance.GetButton(ControlManager.InputCode.SizeButtonMinus, m_globalControllerId);
        if (sizePlus)
        {
            m_sculptBrush.sculptStrength += m_sculptBrush.increaseValueSpeed * (1+m_sculptBrush.sculptStrength * 0.01f) * Time.deltaTime;
            piGUI.setBrushSizeGUI(m_sculptBrush.sculptStrength);
        }
        else
        if (sizeMinus)
        {
            m_sculptBrush.sculptStrength -= m_sculptBrush.increaseValueSpeed * (1+m_sculptBrush.sculptStrength * 0.01f) * Time.deltaTime;
            piGUI.setBrushSizeGUI(m_sculptBrush.sculptStrength);
        }
        //renderColorRadiusScale = renderColorRadiusScale_orig / ( distanceField.distanceFieldRadius * distanceField.distanceFieldRadius + distanceField.distanceFieldRadius);
    }

    void setBrushRoughness()
    {
        //TODO: To implelent Roughness Axis version:
        //ControlManager.Instance.GetAxis(ControlManager.InputCode.RoughnessAxis, m_globalControllerIndex)

        bool roughPlus = ControlManager.Instance.GetButton(ControlManager.InputCode.RoughnessButtonPlus, m_globalControllerId);
        bool roughMinus = ControlManager.Instance.GetButton(ControlManager.InputCode.RoughnessButtonMinus, m_globalControllerId);
        if (roughPlus)
        {
            distanceField.distanceFieldRadius += distanceField.increaseValueSpeed * Time.deltaTime;
            float rad = distanceField.distanceFieldRadius / distanceField.maxRadius;
            //float val = Utils.RemapInterval(0, 1, 1, 5, Mathf.Clamp01(rad));
            piGUI.setScatterControlGUI(rad*10);
        }
        else
        if (roughMinus)
        {
            distanceField.distanceFieldRadius -= distanceField.increaseValueSpeed * Time.deltaTime;
            float rad = distanceField.distanceFieldRadius / distanceField.maxRadius;
            //float val = Utils.RemapInterval(0,1,1,5,Mathf.Clamp01(rad));
            piGUI.setScatterControlGUI(rad*10);
        }
        
    }

    void moveControlPoint()
    {
        Vector3 pos = transform.position;
        m_prevPos = pos;

        Quaternion rot = transform.rotation;
        bool onGUI = false;
        ControlManager.Instance.Get3DPos(ref onGUI, ref pos, ref rot, ref rigidbody, m_moveSensitivity, m_globalControllerId);
        transform.rotation = rot;

        Vector3 sample = pos + m_noiseSeed + Vector3.one * Time.time * brushWarps.sizeNoiseFrequency;
        float scale = brushWarps.posShakeScale * brushWarps.explosionNoise;
        pos.x += Simplex.Noise.Generate(sample.x) / 255 * scale;
        pos.y += Simplex.Noise.Generate(sample.y) / 255 * scale;
        pos.z += Simplex.Noise.Generate(sample.z) / 255 * scale;

        if (onGUI)
        {
            m_sculptBrush.position = pos;
        }
        transform.position = pos;
        m_sculptBrush.brushMoveSpeed = Vector3.Distance(pos, m_prevPos) / Time.deltaTime;
    }

    bool isSculptingCheck()
    {

        //quick hack to have controllerless interactors in the scene that fire on InputCode.FireDemo
        if (isDemoInteractor)
        {
            return sceneRotationSpeed.currentSpeed == sceneRotationSpeed.idleSpeed &&
                ControlManager.Instance.controllers.m_PCInput.GetButton(ControlManager.InputCode.FireDemo, 0);
        }
        else
            return sceneRotationSpeed.currentSpeed == sceneRotationSpeed.idleSpeed && 
                    ControlManager.Instance.GetButton(ControlManager.InputCode.Fire, m_globalControllerId);
    }

#region warning: heavily prototypish scripting
    public void initBrushSizeSliderValue(ref UnityEngine.UI.Slider slider)
    {
        slider.minValue = m_sculptBrush.min;
        slider.maxValue = m_sculptBrush.max;
        slider.value = m_sculptBrush.sculptStrength;
        piGUI.brushSizeSlider = slider;
    }

    public void initScatterControlSliderValue(ref UnityEngine.UI.Slider slider)
    {
        slider.minValue = distanceField.minRadius;
        slider.maxValue = distanceField.maxRadius;
        slider.value = distanceField.distanceFieldRadius;
        piGUI.scatterControlSlider = slider;
    }

    public void OnPhysicsSettingsChanged()
    {
        m_sculptBrush.sculptStrength = m_sculptBrush.defaultStr;
        piGUI.setBrushSizeGUI(m_sculptBrush.sculptStrength);

        distanceField.distanceFieldRadius = MainGame.Instance.physicsSettings.current.particlePhysicsProperties.interactorScatterControl;
        piGUI.setScatterControlGUI(distanceField.distanceFieldRadius / distanceField.maxRadius * 10);
    }

    public void initPositionSliderValue(ref UnityEngine.UI.Slider slider)
    {
        slider.minValue = positionSliderMin;
        slider.maxValue = positionSliderMax;
        slider.value = 0;
        StartCoroutine(doActionNextFrame(()=> { piGUI.initialPosition = transform.localPosition; }));
    }
#endregion

    IEnumerator doActionNextFrame(System.Action action)
    {
        yield return new WaitForEndOfFrame();
        action();
    }
    IEnumerator doAfterFrames(System.Action action, int frames)
    {
        if(frames > 0)
        {
            frames--;
            yield return doAfterFrames(action, frames);
        }

        action();
    }

    public void deleteSelf()
    {
        RenderManager.Instance.interactorData[m_globalGPUInteractorId].flush();
        StartCoroutine(doActionNextFrame(() => { Destroy(gameObject); }));
    }
}
