using UnityEngine;

using System.Collections;

 

public class Test : MonoBehaviour

{

    public ComputeShader Generator;

    public MeshTopology Topology = MeshTopology.Points;

    

    void Start()

    {

        var computedMeshPoints = ComputeMesh();

        CreateMeshFrom(computedMeshPoints);

    }

    

    private Vector3[] ComputeMesh()

    {

        var size = 32*32;

        var buffer = new ComputeBuffer(size, 12, ComputeBufferType.Append);

        Generator.SetBuffer(0, "vertexBuffer", buffer);

        Generator.Dispatch(0, 1, 1, 1);

        var results = new Vector3[size];

        buffer.GetData(results);

        buffer.Dispose();

        return results;

    }

    

    private void CreateMeshFrom(Vector3[] generatedPoints)

    {

        var filter = GetComponent<MeshFilter>();

        //var renderer = GetComponent<MeshRenderer>();

        

        if (generatedPoints.Length > 0)

        {

            var mesh = new Mesh { vertices = generatedPoints };

            var indices = new int[generatedPoints.Length];

 

            for (int i = 0; i < indices.Length; i++)

                indices[i] = i;

            

            mesh.SetIndices(indices, Topology, 0);

            filter.mesh = mesh;

        }

        else

        {

            filter.sharedMesh = null;

        }

    }

}