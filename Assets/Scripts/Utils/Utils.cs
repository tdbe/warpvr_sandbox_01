﻿
public static class Utils {

    //[A, B] --> [a, b]
    //(val - A)*(b-a)/(B-A) + a
    public static float RemapInterval(float A, float B, float newA, float newB, float yourVal)
    {
        return (yourVal - A) * (newB - newA) / (B - A) + newA;
    }
	
}
