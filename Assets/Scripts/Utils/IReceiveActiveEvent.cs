﻿using UnityEngine;
using System.Collections;

public interface IReceiveActiveEvent {

    void ReceiveAwakeEvent(GameObject from, bool status);
    void ReceiveStartEvent(GameObject from, bool status);
}
