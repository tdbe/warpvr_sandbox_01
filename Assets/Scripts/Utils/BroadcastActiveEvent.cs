﻿using UnityEngine;
using System.Collections;

public class BroadcastActiveEvent : MonoBehaviour {

    public GameObject[] targets;

    void Awake()
    {
        foreach (GameObject go in targets)
        {
            IReceiveActiveEvent receiver = go.GetComponent<IReceiveActiveEvent>();
            if (receiver != null)
            {
                receiver.ReceiveAwakeEvent(gameObject, gameObject.activeInHierarchy);
            }
            else
            {
                Debug.LogWarning("IReceiveActiveEvent expected, but this game object has none: "+ go.name);
            }
        }
    }

    void Start()
    {
        foreach (GameObject go in targets)
        {
            IReceiveActiveEvent receiver = go.GetComponent<IReceiveActiveEvent>();
            if (receiver != null)
            {
                receiver.ReceiveStartEvent(gameObject, gameObject.activeInHierarchy);
            }
            else
            {
                Debug.LogWarning("IReceiveActiveEvent expected, but this game object has none: "+ go);
            }
        }
    }
}
