using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.VR;
using UnityEngine.Rendering;

/*
This needs to be cleaned up and be more extensible. Right now it's just me figuring out the tech and prototyping.
*/

public class RenderManager : MonoBehaviourSingleton<RenderManager> 
{

    public Material material;//material which takes care of displaying the particles on screen

    public Camera mainCam;
    //[HideInInspector]
    public Matrix4x4 particleVolumeTRS;


    [SerializeField]
    float m_renderColorRadiusScale;


    int m_warpCount = 10;//The number particles /32
    const int m_warpSize = 32;//32 warps on modern GPUs. each Warp computes data
    int m_particleCount; //== warpSize * warpCount
    ComputeBuffer m_particleBuffer;//store the particles in a GPU compute buffer. This will be a shared buffer across all compute shaders or shaders that have to do with these particles.
    ComputeBuffer m_perParticleVertexBuffer;//a small vertex buffer used on every particle, to draw vertices for a billboard or a cube or a sphere or whatever.

    //This data structure is shared with the compute shader and the render shader. Must declare it the same in those as well.
    struct Particle
    {
        public Vector3 position;
        public Vector4 velocity;
        public Vector3 physxParams;
    };
    public const int interactorArrayLength = 256;//up to 256 active interactors will lag tho. //max allowed number of interactors on the GPU
   

    public struct InteractorData
    {
        public Vector3 target;
        public int isSculpting;
        public float targetStrengh;
        public float sprayOrbRadius;
        public float snoise;
        public float enoise;
        public void flush()
        {
            target = Vector3.zero;
            isSculpting = 0;
            targetStrengh = 0;
            sprayOrbRadius = 0;
            snoise = 0;
            enoise = 0;
        }
    }
    public InteractorData[] interactorData;
    //[SerializeField]
    //GameObject interactorPointObj;
    //Mesh m_interactorPointMesh;
    //Material m_interactorPointMaterial;
  
    GameObject fullScr;

    ComputeBuffer m_interactorDataBuffer;
    CommandBuffer m_particleCommandBuffer = null;
    //CommandBuffer m_GUIOverlayCommandBuffer = null;

    bool m_flushCSValues = false;

    Color m_cameraBgDefaultColor;
    float m_cameraBgDefaultBrightness = .8f;

    
    


    void Start()
    {
        //

        if (mainCam == null)
        {
            mainCam = Camera.main;
        }

        m_cameraBgDefaultColor = mainCam.backgroundColor;
        scaleBgColor(m_cameraBgDefaultBrightness);

        constructParticleBuffer();
        constructCommandBuffers();
        //Render();

    }

    public void flushParticleVolumeCSValues()
    {
        m_flushCSValues = true;

    }

    void constructCommandBuffers()
    {
        m_particleCommandBuffer = new CommandBuffer();
        m_particleCommandBuffer.name = "particles";
        //m_GUIOverlayCommandBuffer = new CommandBuffer();
        //m_GUIOverlayCommandBuffer.name = "GUI-Overlay";
        mainCam.RemoveAllCommandBuffers();
        mainCam.AddCommandBuffer(CameraEvent.AfterFinalPass, m_particleCommandBuffer);
        //mainCam.AddCommandBuffer(CameraEvent.AfterForwardAlpha, m_GUIOverlayCommandBuffer);
        //mainCam.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, m_commandBuffer);
        //mainCam.AddCommandBuffer(CameraEvent.BeforeForwardAlpha, commandBuffer);

        //RenderTargetIdentifier[] mrt = { BuiltinRenderTextureType.GBuffer0 };
        //buff.SetRenderTarget(mrt, BuiltinRenderTextureType.CameraTarget);



    }

    void Render()
    {

        //activate render pass 0 on our particle render shader
        //material.SetPass(0);

        //do a draw call using the buffer in this class instead of a vertexbufferObject
        //Graphics.DrawProcedural(MeshTopology.Points, 1, particleCount);
        //Graphics.DrawProcedural(MeshTopology.Triangles, 36, particleCount);
        //Graphics.DrawProcedural(MeshTopology.Triangles, 6, particleCount);

        m_particleCommandBuffer.Clear();

        particleVolumeTRS = Matrix4x4.TRS(
                            MainGame.Instance.particleRootPos.transform.position,
                            MainGame.Instance.particleRootPos.transform.rotation,
                            MainGame.Instance.particleRootPos.transform.localScale
                            );

        m_particleCommandBuffer.DrawProcedural(
            particleVolumeTRS, 
            material, 
            0, 
            MeshTopology.Triangles, 
            6, 
            m_particleCount
        );


        //m_GUIOverlayCommandBuffer.DrawMesh()
    }

    public void resetParticleBuffer()
    {
        reconstructParticleBuffer();
        flushParticleVolumeCSValues();
    }

    Particle[] createParticleShape()
    {
        int num = 60;// Mathf.FloorToInt(Mathf.Pow(particleCount, -3));// 60;//6
        //particleCount = warpCount * warpSize;
        m_particleCount = Mathf.FloorToInt(Mathf.Pow(num, 3));// 216000;
        m_warpCount = m_particleCount / m_warpSize;
        Debug.Log(m_particleCount);

        // Init particles
        Particle[] particleArray = new Particle[m_particleCount];
        /*
		for (int i = 0; i < particleCount; ++i)
		{
			particleArray[i].position = Random.insideUnitSphere * initialSpread;
			particleArray[i].velocity = particleArray[i].position.normalized;
		}
		*/

        float spacing = 1f * MainGame.Instance.particleRootPos.transform.localScale.x;//0.016666f // 1f;//2f
        int i = 0;


        Vector3 particleVolumeOffset = Vector3.one * spacing * num / 2;
        ////offset.z *= -3;
        //offset = MainGame.Instance.particleRootPos.transform.position - offset;

        //making and initializing a cube of particles
        for (int x = 0; x < num; x++)
        {
            for (int y = 0; y < num; y++)
            {
                for (int z = 0; z < num; z++)
                {

                    particleArray[i].position = (new Vector3(x * spacing, y * spacing, z * spacing) - particleVolumeOffset);
                    //particleArray[i].position = Random.insideUnitSphere * initialSpread;
                    particleArray[i].velocity = Vector3.zero;// particleArray[i].position.normalized;
                    particleArray[i].velocity.w = 0;
                    particleArray[i].physxParams = particleArray[i].position;// new Vector4(particleArray[i].position.x, particleArray[i].position.y, particleArray[i].position.z, 0);
                    i++;
                }

            }
        }

        return particleArray;
    }

    void constructParticleBuffer()
    {
        Particle[] particleArray = createParticleShape();

        //The particle buffer
        m_particleBuffer = new ComputeBuffer(m_particleCount, 40);//44 // stride = sizeof(Particle)
        m_particleBuffer.SetData(particleArray);

        //the length of a billboard or cube around the point. (point will be 0,0,0)
        float segmentLength = 1f * MainGame.Instance.particleRootPos.transform.localScale.x;// 0.016666f // 0.9f;//1.9f;//should be the same as "spacing"; unless you want the cubes to be smaller than the grid
        //Vector3[] cubeVerts = makeCubeVerts(segmentLength, cubeShape);
        Vector3[] quadVerts = makeQuadVerts(segmentLength);

        //cubedVertBuffer = new ComputeBuffer(36, 12);
        //perParticleVertexBuffer = new ComputeBuffer(6, 12);//6 float3's
        m_perParticleVertexBuffer = new ComputeBuffer(6, 12);
        m_perParticleVertexBuffer.SetData(quadVerts);
        material.SetBuffer(MainGame.ShaderKeywords._QuadVerts, m_perParticleVertexBuffer);

        // bind the buffer to both the compute shader and the display shader.
        for(int j=0; j < MainGame.Instance.physicsSettings.computeEntities.Length; j++)
        {
            MainGame.Instance.physicsSettings.computeEntities[j].cs.SetBuffer(0, MainGame.CSKeywords._ParticleBuffer, m_particleBuffer);
        }
        material.SetBuffer(MainGame.ShaderKeywords._ParticleBuffer, m_particleBuffer); 

        //compute buffer for information stored on a per-interactor basis.
        interactorData = new InteractorData[interactorArrayLength];
        m_interactorDataBuffer = new ComputeBuffer(interactorArrayLength, 24); // stride = sizeof(struct)
        //computeFloatsBuffer.SetData(computeFloats);
        for (int k = 0; k < MainGame.Instance.physicsSettings.computeEntities.Length; k++)
        {
            MainGame.Instance.physicsSettings.computeEntities[k].cs.SetBuffer(0, MainGame.CSKeywords._InteractorData, m_interactorDataBuffer);
            MainGame.Instance.physicsSettings.computeEntities[k].cs.SetInt(MainGame.CSKeywords._InteractorCount, interactorArrayLength); //TODO: is there a buffer.Length in HLSL? Yes. GetDimensions().numStructs
        }

    }

    void reconstructParticleBuffer()
    {
        Particle[] particleArray = createParticleShape();
        m_particleBuffer.SetData(particleArray);


        // bind the buffer to both the compute shader and the display shader.
        for (int j = 0; j < MainGame.Instance.physicsSettings.computeEntities.Length; j++)
        {
            MainGame.Instance.physicsSettings.computeEntities[j].cs.SetBuffer(0, MainGame.CSKeywords._ParticleBuffer, m_particleBuffer);
        }
        material.SetBuffer(MainGame.ShaderKeywords._ParticleBuffer, m_particleBuffer);
        /*
        //computeFloatsBuffer.SetData(computeFloats);
        for (int k = 0; k < MainGame.Instance.physicsSettings.computeEntities.Length; k++)
        {
            MainGame.Instance.physicsSettings.computeEntities[k].cs.SetBuffer(0, MainGame.CSKeywords._InteractorData, interactorDataBuffer);
            MainGame.Instance.physicsSettings.computeEntities[k].cs.SetInt(MainGame.CSKeywords._InteractorCoun, interactorArrayLength); //TODO: is there a buffer.Length in HLSL? Yes. GetDimensions().numStructs
        }
        */
    }

    void FixedUpdate()
    {

      
    }

    void Update()
    {
        //Set material shader and compute shader arrays only once per frame, as opposed to once per interactor modification for each interactor.
        setRenderFloats();
        setComputeFloats();
        dispatchCurrentComputeShader();


    }

    void LateUpdate()
    {
       
    }

    void dispatchCurrentComputeShader()
    {
        MainGame.Instance.physicsSettings.current.cs.Dispatch(0, m_warpCount, 1, 1);
    }

    void setRenderFloats()
    {
        //TODO: this needs to be on a per-interactor basis
        material.SetFloat(MainGame.ShaderKeywords._ColorRadiusScale, m_renderColorRadiusScale);
        //material.SetFloatArray(renderFloats._ColorRadiusScale.name, renderFloats._ColorRadiusScale.array);
        //material.SetColorArray(renderFloats._SpeedColor.name, renderFloats._SpeedColor.array);
    }

    void setComputeFloats()
    {
        if (m_flushCSValues)
        {
            m_flushCSValues = false;
            MainGame.Instance.physicsSettings.current.cs.SetInt(MainGame.CSKeywords._Flush, 1);
        }
        else
            MainGame.Instance.physicsSettings.current.cs.SetInt(MainGame.CSKeywords._Flush, 0);

        //physics_computeShader.SetInt(MainGame.CSKeywords._InteractorCount, interactorArrayLength);//uncomment this if you change interactorArrayLength on the fly
        MainGame.Instance.physicsSettings.current.cs.SetFloat(MainGame.CSKeywords._Drag, MainGame.Instance.physicsSettings.current.particlePhysicsProperties.drag);
        MainGame.Instance.physicsSettings.current.cs.SetFloat(MainGame.CSKeywords._VerletElasticity, MainGame.Instance.physicsSettings.current.particlePhysicsProperties.verletElasticity);
        //MainGame.Instance.physicsSettings.current.cs.SetFloat("deltaTime", Time.deltaTime);

        m_interactorDataBuffer.SetData(interactorData);

    }


    //void OnWillRenderObject()
    void OnRenderObject()
	{
        
        //updating the drawProcedural method of commnand buffer instead, to update the matrix TRS transformation
        Render();
        
    }


    public new void OnDestroy()
    {
        if(mainCam!=null)
            mainCam.RemoveAllCommandBuffers();
        else if (Camera.main != null)
        {
            Camera.main.RemoveAllCommandBuffers();
        }

        //clear the GPU buffer
        m_particleBuffer.Release();
        m_interactorDataBuffer.Release();
        m_perParticleVertexBuffer.Release();
        
    }

    public void scaleBgColor(float val)
    {
        mainCam.backgroundColor = m_cameraBgDefaultColor * val;
    }

    public float getCamBgDefaultBrightness()
    {
        return m_cameraBgDefaultBrightness;
    }

    void sendRotationMatrixToShader(Material mat, Transform obj)
    {
        Matrix4x4 rot_m = Matrix4x4.TRS(Vector3.zero, obj.rotation, Vector3.one);
        mat.SetMatrix(MainGame.ShaderKeywords._TargetRotationMatrix, rot_m);
    }

    Vector3[] makeQuadVerts(float segmentLength)
    {
        Vector3[] quadShape = getAQuadShape(segmentLength);

        return new Vector3[]
		{
            quadShape[0], quadShape[1], quadShape[2],
            quadShape[3], quadShape[4], quadShape[5]

        };
    }

    Vector3[] getAQuadShape(float cubeSegmentLength)
    {
        Vector3[] quadShape = new Vector3[8];
        
        // 
        quadShape[0] = Vector3.zero;
        quadShape[0].x -= cubeSegmentLength / 2;
        quadShape[0].y += cubeSegmentLength / 2;

        // 
        quadShape[1] = Vector3.zero;
        quadShape[1].x += cubeSegmentLength / 2;
        quadShape[1].y += cubeSegmentLength / 2;

        // 
        quadShape[2] = Vector3.zero;
        quadShape[2].x += cubeSegmentLength / 2;
        quadShape[2].y -= cubeSegmentLength / 2;

        // 
        quadShape[3] = Vector3.zero;
        quadShape[3].x += cubeSegmentLength / 2;
        quadShape[3].y -= cubeSegmentLength / 2;

        // 
        quadShape[4] = Vector3.zero;
        quadShape[4].x -= cubeSegmentLength / 2;
        quadShape[4].y -= cubeSegmentLength / 2;

        // 
        quadShape[5] = Vector3.zero;
        quadShape[5].x -= cubeSegmentLength / 2;
        quadShape[5].y += cubeSegmentLength / 2;

        return quadShape;
    }

    Vector3[] makeCubeVerts(float cubeSegmentLength)
    {
        Vector3[] cubeShape = getACubeShape(cubeSegmentLength);

        return new Vector3[]//[24];
		{/*
            cubeShape[7], cubeShape[4], cubeShape[5],
            cubeShape[5], cubeShape[6], cubeShape[7]
            */
            
			cubeShape[0], cubeShape[1], cubeShape[2],
			cubeShape[2], cubeShape[3], cubeShape[0],
			cubeShape[1], cubeShape[5], cubeShape[6],
			cubeShape[6], cubeShape[2], cubeShape[1],
			
            
			cubeShape[5], cubeShape[4], cubeShape[7],
			cubeShape[7], cubeShape[6], cubeShape[5],
			cubeShape[4], cubeShape[0], cubeShape[3],
			cubeShape[3], cubeShape[7], cubeShape[4],
			
			cubeShape[3], cubeShape[2], cubeShape[6],
            cubeShape[6], cubeShape[7], cubeShape[3],
            cubeShape[5], cubeShape[1], cubeShape[0],
            cubeShape[0], cubeShape[4], cubeShape[5]
        };
    }

    Vector3[] getACubeShape(float cubeSegmentLength)
    {
        Vector3[] cubeShape = new Vector3[8];
        // front bott left
        cubeShape[0] = Vector3.zero;
        cubeShape[0].x += cubeSegmentLength / 2;
        cubeShape[0].y += cubeSegmentLength / 2;
        cubeShape[0].z -= cubeSegmentLength / 2;
        // back bott left
        cubeShape[1] = Vector3.zero;
        cubeShape[1].x += cubeSegmentLength / 2;
        cubeShape[1].y -= cubeSegmentLength / 2;
        cubeShape[1].z -= cubeSegmentLength / 2;
        // back top left
        cubeShape[2] = Vector3.zero;
        cubeShape[2].x -= cubeSegmentLength / 2;
        cubeShape[2].y -= cubeSegmentLength / 2;
        cubeShape[2].z -= cubeSegmentLength / 2;
        // front top left
        cubeShape[3] = Vector3.zero;
        cubeShape[3].x -= cubeSegmentLength / 2;
        cubeShape[3].y += cubeSegmentLength / 2;
        cubeShape[3].z -= cubeSegmentLength / 2;

        // front bott right
        cubeShape[4] = Vector3.zero;
        cubeShape[4].x += cubeSegmentLength / 2;
        cubeShape[4].y += cubeSegmentLength / 2;
        cubeShape[4].z += cubeSegmentLength / 2;
        // back bott right
        cubeShape[5] = Vector3.zero;
        cubeShape[5].x += cubeSegmentLength / 2;
        cubeShape[5].y -= cubeSegmentLength / 2;
        cubeShape[5].z += cubeSegmentLength / 2;
        // back top right
        cubeShape[6] = Vector3.zero;
        cubeShape[6].x -= cubeSegmentLength / 2;
        cubeShape[6].y -= cubeSegmentLength / 2;
        cubeShape[6].z += cubeSegmentLength / 2;
        // front top right
        cubeShape[7] = Vector3.zero;
        cubeShape[7].x -= cubeSegmentLength / 2;
        cubeShape[7].y += cubeSegmentLength / 2;
        cubeShape[7].z += cubeSegmentLength / 2;

        return cubeShape;
    }

    //public GameObject circle;
    /*
	void createCircle(){
		float theta_scale = 0.5f;
		int size = (int)((2.0f * Mathf.PI) / theta_scale)+1;
		//float radius = 2;

		LineRenderer lineRenderer = circle.gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
		//lineRenderer.SetColors(c1, c2);
		lineRenderer.SetWidth(0.2F, 0.2F);
		lineRenderer.SetVertexCount(size);
	}
	void drawCircle(float radius){

		int i = 0;
		for(float theta = 0f; theta < 2 * Mathf.PI; theta += 0.5f) {
			float x = radius*Mathf.Cos(theta);
			float y = radius*Mathf.Sin(theta);
			//Debug.DrawRay(cameraZ.transform.position, cameraZ.transform.forward);
			Vector3 pos = new Vector3(circle.transform.position.x + cameraZ.transform.forward.x * x, circle.transform.position.y + cameraZ.transform.forward.y * y, circle.transform.position.z);
			circle.GetComponent<LineRenderer>().SetPosition(i, pos);
			i+=1;
		}
	}
	*/

    
}
