
Latest Build: https://bitbucket.org/tdbe/warpvr_sandbox_01/downloads/latestBuild.rar (Only works on DirectX 11+ hardware and OS)

Youtube video: https://youtu.be/FhKOnyaAyXM

---

Experimenting with sculpting for VR.

Working on a Vive / Oculus sculpting / fluid simulation tool.

I'll use deferred raytraced particles into screenspace meta volumes, or something like that. Some kind of unconstrained volume redering / sampling is desired.

Particle processing done with HLSL DX11+ compute shaders.

Unity 5.4+

For now, the project is under the following Creative Commons license: Attribution - NonCommercial - NoDerivs https://creativecommons.org/licenses/by-nc-nd/4.0/ 

Basically, if you are senpai, let's contribute to the same project.



